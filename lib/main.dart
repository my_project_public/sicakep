import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/controllers/main_bottom_navbar_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var status = prefs.getBool('isLogin') ?? false;

  Get.put(MainBottomNavbarController());
  runApp(MyApp(status));
}

// ignore: use_key_in_widget_constructors, must_be_immutable
class MyApp extends StatefulWidget {
  bool status = false;
  MyApp(this.status, {Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String title = "SiCakep";
  String content = "content";
  @override
  void initState() {
    super.initState();
    _loadToken();
  }

  _loadToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('token') != null) {
      // setState(() {
      //   route1 = '/home';
      // });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: title,
      initialRoute: (widget.status) ? Routes.HOME : Routes.WELCOME,
      getPages: AppPages.routes,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xFF8F02FD),
        fontFamily: 'nunito',
        scaffoldBackgroundColor: Colors.white,
      ),
    );
  }
}
