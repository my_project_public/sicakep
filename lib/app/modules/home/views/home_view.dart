// ignore_for_file: unused_local_variable

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/main_bottom_navbar.dart';
import 'package:url_launcher/url_launcher.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  HomeView({Key? key}) : super(key: key);

  ScrollController scrollController = ScrollController();

  @override
  // ignore: override_on_non_overriding_member
  void initState() {
    // controller.getNews("0");
  }

  void onScroll() {
    double maxScroll = scrollController.position.maxScrollExtent;
    double currentScroll = scrollController.position.pixels;

    print(currentScroll);
    print("currentScroll");

    print(maxScroll);
    print("maxScroll");

    if (currentScroll == maxScroll) {
      print("lol");
      print(currentScroll);
      print(maxScroll);
      controller.addNews(controller.news1.value.length.toString());
    }

    currentScroll = maxScroll;
  }

  Future<void> _launchURL(String url1) async {
    Uri url = Uri.parse(url1);

    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    scrollController.addListener(onScroll);

    return Scaffold(
      bottomNavigationBar: MainBottomNavbar(),
      body: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          children: [
            // appbar
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 24, vertical: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'SiCakep',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Container(
                        width: 260,
                        child: Text(
                          'Sistem Informasi Cepat Tanggapi Kekerasan Perempuan dan anak',
                          style: TextStyle(
                              fontSize: 13,
                              color: AppColor.grey,
                              decoration: TextDecoration.underline),
                        ),
                      )
                    ],
                  ),
                  IconButton(
                    onPressed: () {
                      print("object");
                      Get.toNamed(Routes.HISTORY_LAPORAN);
                    },
                    icon: Icon(
                      Icons.history,
                      color: Colors.blue,
                      size: 30.0,
                      semanticLabel: 'Text to announce in accessibility modes',
                    ),
                  ),
                ],
              ),
            ),
            // Jenis kekerasan
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 24, bottom: 10),
                    child: Text(
                      'Jenis Kekerasan',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                  ),
                  Obx(() {
                    if (controller.isLoading.value) {
                      return const Center(child: CircularProgressIndicator());
                    } else {
                      return SizedBox(
                          height: 150,
                          child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            physics: BouncingScrollPhysics(),
                            padding: EdgeInsets.symmetric(horizontal: 24),
                            itemBuilder: (context, index) {
                              return MenuItem(
                                title: controller.category.value[index].judul,
                                image: controller.category.value[index].url,
                                desc: controller.category.value[index].desc,
                                onTap: () {
                                  List temp1 = [
                                    controller.category.value[index].judul,
                                    controller.category.value[index].url,
                                    controller.category.value[index].desc
                                  ];
                                  String temp = json.encode(temp1);
                                  Get.toNamed(
                                    Routes.DETAIL_KEKERASAN,
                                    arguments: temp,
                                  );
                                },
                              );
                            },
                            itemCount: controller.category.value.length,
                          ));
                    }
                  }),
                ],
              ),
            ),
            // Berita Terkini
            Container(
              margin: EdgeInsets.only(top: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 24, bottom: 10),
                    child: Text(
                      'Berita Terkini',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                  ),
                  Obx(() {
                    if (controller.isLoadingNews.value) {
                      return Container(
                        margin: EdgeInsets.only(top: 20),
                        child: ListView.builder(
                          primary: false,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return Center(child: CircularProgressIndicator());
                          },
                          itemCount: controller.isLoadingAddNews.value
                              ? (controller.news1.value != null)
                                  ? controller.news1.value.length + 10
                                  : 10
                              : (controller.news1.value != null)
                                  ? controller.news1.value.length + 10
                                  : 10,
                        ),
                      );
                    } else if (controller.news1.value.length > 0) {
                      return ListView.builder(
                        itemCount: controller.news1.value.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        padding:
                            EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                        itemBuilder: (context, index) {
                          return NewsTile(
                            onTap: () {
                              List temp1 = [
                                controller.news1.value[index].judul,
                                controller.news1.value[index].url,
                                controller.news1.value[index].desc ?? "-",
                                controller.news1.value[index].image
                              ];
                              String temp = json.encode(temp1);
                              print(temp);
                              Get.toNamed(
                                Routes.DETAIL_BERITA,
                                arguments: temp,
                              );
                            },
                            title: controller.news1.value[index].judul,
                            date: controller.news1.value[index].created_at,
                            image: controller.news1.value[index].image,
                          );
                        },
                      );
                    } else {
                      // ignore: curly_braces_in_flow_control_structures, avoid_unnecessary_containers
                      return Container(child: Text("Terjadi Kesalahan!"));
                    }
                  }),
                  const SizedBox(height: 40)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NewsTile extends StatelessWidget {
  final String title;
  final String date;
  final String image;
  final void Function() onTap;

  NewsTile({
    required this.onTap,
    required this.title,
    required this.date,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      borderRadius: BorderRadius.circular(18),
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.transparent,
        margin: EdgeInsets.only(bottom: 10),
        child: Row(
          children: [
            Container(
              width: 130,
              height: 110,
              margin: EdgeInsets.only(right: 16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                image: DecorationImage(
                  image: NetworkImage(image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Text(
                    date,
                    style: TextStyle(color: AppColor.grey),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MenuItem extends StatelessWidget {
  final void Function() onTap;
  final String title;
  final String desc;

  final String image;

  MenuItem({
    required this.onTap,
    required this.title,
    required this.desc,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: 120,
        height: 150,
        margin: EdgeInsets.only(right: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(18),
        ),
        child: Column(
          children: [
            Container(
              width: 120,
              height: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(18),
                ),
                image: DecorationImage(
                  image: NetworkImage(image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              width: 120,
              height: 50,
              alignment: Alignment.center,
              child: Text(
                '$title',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
