import 'package:get/get.dart';
import 'package:ppa_lapor/app/modules/pengaturan/controllers/pengaturan_controller.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
  }
}
