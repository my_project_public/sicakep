import 'package:get/get.dart';
import 'package:ppa_lapor/models/models.dart';
import 'package:ppa_lapor/services/services.dart';

class HomeController extends GetxController {
  final count = 0.obs;
  @override
  void onInit() {
    getCategory();
    getNews("0");
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void increment() => count.value++;
  List<News> x = [];
  ApiReturnValue category =
      ApiReturnValue(value: null, message: null, statusCode: null);
  ApiReturnValue news1 =
      ApiReturnValue(value: null, message: null, statusCode: null);

  RxBool isLoading = RxBool(false);

  RxBool isLoadingNews = RxBool(false);
  RxBool isLoadingAddNews = RxBool(false);

  void getCategory() async {
    try {
      isLoading.value = true;
      ApiReturnValue category1 =
          await CategoryServices.getCategory() as ApiReturnValue;
      category = category1;
    } finally {
      isLoading.value = false;
    }
  }

  void getNews(String skip) async {
    try {
      isLoadingNews.value = true;
      ApiReturnValue category1 =
          await NewsServices.getNews(skip) as ApiReturnValue;
      news1 = category1;
    } finally {
      isLoadingNews.value = false;
    }
  }

  void addNews(String skip) async {
    try {
      isLoadingAddNews.value = true;
      ApiReturnValue category1 =
          await NewsServices.getNews(skip) as ApiReturnValue;
      print(category1.value.length);
      if (category1.value.length > 0) {
        print("category1");
        print(category1);
        List<News> items = category1.value;
        news1.value.addAll(items);
      }
    } finally {
      isLoadingAddNews.value = false;
    }
  }
}
