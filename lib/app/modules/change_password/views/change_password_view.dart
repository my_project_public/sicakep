import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/alert/custom_alert.dart';
import 'package:ppa_lapor/app/widgets/custom_input.dart';

import '../controllers/change_password_controller.dart';

class ChangePasswordView extends GetView<ChangePasswordController> {
  const ChangePasswordView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        title: Text(
          'Edit Profile',
          style: TextStyle(
            fontSize: 24,
            color: Colors.black,
            fontWeight: FontWeight.w600,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border:
                  Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: (controller.isLoading.value == false)
          ? Center(child: CircularProgressIndicator())
          : ListView(
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
              children: [
                // section 1 - Profile Picture
                Obx(() => (controller.isLoading2.value)
                    ? Center(
                        child: Stack(
                          children: [
                            (controller.image == null)
                                ? ClipOval(
                                    child: Container(
                                      width: 98,
                                      height: 98,
                                      decoration: BoxDecoration(
                                        color: AppColor.grey,
                                        borderRadius:
                                            BorderRadius.circular(100),
                                      ),
                                    ),
                                  )
                                : Container(
                                    width: 98,
                                    height: 98,
                                    child: Container(
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                                image: FileImage(
                                                    controller.image!),
                                                fit: BoxFit.cover)))),
                            Positioned(
                              bottom: 0,
                              right: 0,
                              child: SizedBox(
                                width: 36,
                                height: 36,
                                child: ElevatedButton(
                                  onPressed: () {
                                    controller.pickImage();
                                  },
                                  child: Icon(
                                    Icons.camera_alt_outlined,
                                    color: Colors.white,
                                  ),
                                  style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : Center(child: CircularProgressIndicator())),
                CustomInput(
                  cek: false,
                  cekArray: false,
                  controller: controller.nameC,
                  label: 'Nama*',
                  hint: controller.nameC.text,
                  margin: EdgeInsets.only(bottom: 16, top: 16),
                ),
                CustomInput(
                  controller: controller.emailC,
                  label: 'Email*',
                  hint: 'Masukkan Email',
                  cek: false,
                  cekArray: false,
                  prefixIcon: Icon(Icons.mail, color: AppColor.grey),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                CustomInput(
                  controller: controller.phoneC,
                  label: 'No HP*',
                  hint: '08XXXXXXXXXX',
                  cek: false,
                  cekArray: false,
                  prefixIcon: Icon(Icons.phone, color: AppColor.grey),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                CustomInput(
                  maxLines: null,
                  controller: controller.ttlC,
                  label: 'Tanggal Lahir*',
                  hint: '08XXXXXXXXXX',
                  cek: true,
                  cekArray: false,
                  prefixIcon: Icon(Icons.phone, color: AppColor.grey),
                  margin: EdgeInsets.only(bottom: 16),
                ),

                Text(
                  'Agama*',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'inter',
                  ),
                ),
                SizedBox(height: 4),
                Obx(
                  () => SizedBox(
                    height: 48,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton2(
                        buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                        buttonDecoration: BoxDecoration(
                          border: Border.all(color: AppColor.grey, width: 1),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        hint: Text(
                          'Select Item',
                          style: TextStyle(
                              fontSize: 14, color: Theme.of(context).hintColor),
                        ),
                        items: controller.itemsAgama
                            .map((item) => DropdownMenuItem<String>(
                                  value: item,
                                  child: Text(item),
                                ))
                            .toList(),
                        value: controller.selectedAgamaValue.value,
                        onChanged: (value) {
                          controller.selectedAgamaValue.value = value as String;
                        },
                        buttonHeight: 40,
                        buttonWidth: 140,
                        itemHeight: 40,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 4),

                Text(
                  'Jenis Kelamin*',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'inter',
                  ),
                ),
                SizedBox(height: 4),
                Obx(
                  () => SizedBox(
                    height: 48,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton2(
                        buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                        buttonDecoration: BoxDecoration(
                          border: Border.all(color: AppColor.grey, width: 1),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        hint: Text(
                          'Select Item',
                          style: TextStyle(
                              fontSize: 14, color: Theme.of(context).hintColor),
                        ),
                        items: controller.items
                            .map((item) => DropdownMenuItem<String>(
                                  value: item,
                                  child: Text(item),
                                ))
                            .toList(),
                        value: controller.selectedValue.value,
                        onChanged: (value) {
                          controller.selectedValue.value = value as String;
                        },
                        buttonHeight: 40,
                        buttonWidth: 140,
                        itemHeight: 40,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 4),

                Text(
                  'Sudah Menikah?*',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'inter',
                  ),
                ),
                SizedBox(height: 4),

                Obx(
                  () => SizedBox(
                    height: 48,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton2(
                        buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                        buttonDecoration: BoxDecoration(
                          border: Border.all(color: AppColor.grey, width: 1),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        hint: Text(
                          'Status Nikah',
                          style: TextStyle(
                              fontSize: 14, color: Theme.of(context).hintColor),
                        ),
                        items: controller.itemsNikah
                            .map((item) => DropdownMenuItem<String>(
                                  value: item.id,
                                  child: Text(item.name),
                                ))
                            .toList(),
                        value: controller.selectedNikahValue.value,
                        onChanged: (value) {
                          controller.selectedNikahValue.value = value as String;
                        },
                        buttonHeight: 40,
                        buttonWidth: 140,
                        itemHeight: 40,
                      ),
                    ),
                  ),
                ),
                CustomInput(
                  cek: false,
                  cekArray: false,
                  controller: controller.alamatC,
                  maxLines: null,
                  label: 'Alamat*',
                  hint: 'Masukkan alamat anda',
                  margin: EdgeInsets.only(bottom: 16, top: 16),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Text(
                    'Isi Form dibawah ini jika ingin mengubah password!',
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'inter',
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                SizedBox(height: 15),

                Obx(
                  () => CustomInput(
                    cek: false,
                    cekArray: false,
                    controller: controller.passwordLamaC,
                    label: 'Password Lama*',
                    hint: '***********',
                    obsecureText: controller.obsecureText.value,
                    prefixIcon: Container(
                      padding: EdgeInsets.all(16),
                      child: SvgPicture.asset('assets/icons/Lock.svg',
                          color: AppColor.grey),
                    ),
                    suffixIcon: IconButton(
                      icon: (controller.obsecureText.value != false)
                          ? Icon(
                              Icons.visibility,
                              color: AppColor.grey,
                            )
                          : Icon(
                              Icons.visibility_off,
                              color: AppColor.grey,
                            ),
                      onPressed: () {
                        controller.obsecureText.value =
                            !(controller.obsecureText.value);
                      },
                      color: AppColor.grey,
                    ),
                    margin: EdgeInsets.only(bottom: 16),
                  ),
                ),
                Obx(
                  () => CustomInput(
                    cek: false,
                    cekArray: false,
                    controller: controller.passwordBaruC,
                    label: 'Password Baru*',
                    hint: '***********',
                    obsecureText: controller.obsecureText.value,
                    prefixIcon: Container(
                      padding: EdgeInsets.all(16),
                      child: SvgPicture.asset('assets/icons/Lock.svg',
                          color: AppColor.grey),
                    ),
                    suffixIcon: IconButton(
                      icon: (controller.obsecureText.value != false)
                          ? Icon(
                              Icons.visibility,
                              color: AppColor.grey,
                            )
                          : Icon(
                              Icons.visibility_off,
                              color: AppColor.grey,
                            ),
                      onPressed: () {
                        controller.obsecureText.value =
                            !(controller.obsecureText.value);
                      },
                      color: AppColor.grey,
                    ),
                    margin: EdgeInsets.only(bottom: 16),
                  ),
                ),
                Obx(
                  () => CustomInput(
                    cek: false,
                    cekArray: false,
                    controller: controller.passwordBaruKonfirmasiC,
                    label: 'Konfirmasi Password Baru*',
                    hint: '***********',
                    obsecureText: controller.obsecureText.value,
                    prefixIcon: Container(
                      padding: EdgeInsets.all(16),
                      child: SvgPicture.asset('assets/icons/Lock.svg',
                          color: AppColor.grey),
                    ),
                    suffixIcon: IconButton(
                      icon: (controller.obsecureText.value != false)
                          ? Icon(
                              Icons.visibility,
                              color: AppColor.grey,
                            )
                          : Icon(
                              Icons.visibility_off,
                              color: AppColor.grey,
                            ),
                      onPressed: () {
                        controller.obsecureText.value =
                            !(controller.obsecureText.value);
                      },
                      color: AppColor.grey,
                    ),
                    margin: EdgeInsets.only(bottom: 32),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 48,
                  child: GestureDetector(
                    onTap: () {},
                    child: ElevatedButton(
                      child: Text(
                        'Update Profile',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'inter'),
                      ),
                      onPressed: () {
                        print("asasasasas");
                        // if ((controller.isLoading1.value == true)) {
                        //   return;
                        // }
                        if (controller.nameC.text.length < 1) {
                          CustomAlert.show(
                            title: 'Nama',
                            cek: true,
                            description: "Nama Tidak Boleh kosong!",
                            onTap: () {},
                          );
                          return;
                        }
                        if (controller.ttlC.text.length < 1) {
                          CustomAlert.show(
                            title: 'Tanggal Lahir',
                            cek: true,
                            description: "Tanggal Lahir tidak boleh kosong!",
                            onTap: () {},
                          );
                          return;
                        }
                        if (controller.selectedAgamaValue.value.length < 1) {
                          CustomAlert.show(
                            title: 'Jenis Kelamin',
                            cek: true,
                            description: "Jenis Kelamin tidak boleh kosong!",
                            onTap: () {},
                          );
                          return;
                        }
                        if (controller.selectedValue.value.length < 1) {
                          CustomAlert.show(
                            title: 'Agama',
                            cek: true,
                            description: "Agama tidak boleh kosong!",
                            onTap: () {},
                          );
                          return;
                        }
                        if (controller.alamatC.text.length < 1) {
                          CustomAlert.show(
                            title: 'Alamat',
                            cek: true,
                            description: "Alamat tidak boleh kosong!",
                            onTap: () {},
                          );
                          return;
                        }
                        controller.requestUpdate.name = controller.nameC.text;
                        controller.requestUpdate.ttl = controller.ttlC.text;
                        controller.requestUpdate.email = controller.emailC.text;
                        controller.requestUpdate.gender =
                            controller.selectedValue.value;
                        controller.requestUpdate.alamat =
                            controller.alamatC.text;
                        controller.requestUpdate.agama =
                            controller.selectedAgamaValue.value;
                        controller.requestUpdate.isMerried =
                            controller.selectedNikahValue.value;
                        controller.requestUpdate.phone = controller.phoneC.text;
                        controller.requestUpdate.image = controller.image;
                        controller.requestUpdate.passwordLamaC =
                            controller.passwordLamaC.text;
                        controller.requestUpdate.passwordBaruC =
                            controller.passwordBaruC.text;
                        controller.requestUpdate.passwordBaruKonfirmasiC =
                            controller.passwordBaruKonfirmasiC.text;

                        controller.updateProfile(controller.requestUpdate);
                        Future.delayed(Duration(milliseconds: 5000), () {
                          print("code");
                          print(controller.userUpdate.statusCode);
                          if (controller.isLoading1.value == false) {
                            print(controller.userUpdate.statusCode);
                            if (controller.userUpdate.statusCode == 200) {
                              CustomAlert.show(
                                title: 'Success',
                                description: "Update Profile Success",
                                onTap: () {
                                  Get.delete<ChangePasswordController>();
                                  Get.toNamed(Routes.HOME);
                                },
                              );
                            } else {
                              CustomAlert.show(
                                title: 'Register Gagal',
                                description:
                                    "Email atau Nomor handphone sudah digunakan",
                                onTap: () {},
                              );
                            }
                          } else {}
                        });
                      },
                      style: ElevatedButton.styleFrom(
                        primary: AppColor.primaryColor,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18)),
                      ),
                    ),
                  ),
                )
              ],
            ),
    );
  }
}
