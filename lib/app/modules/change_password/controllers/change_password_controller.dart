import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ppa_lapor/models/models.dart';
import 'package:ppa_lapor/services/services.dart';

class ChangePasswordController extends GetxController {
  final ImagePicker picker = ImagePicker();
  File? image;

  UpdateProfileRequest requestUpdate = new UpdateProfileRequest();

  String? img;

  RxBool obsecureText = RxBool(true);
  TextEditingController passwordLamaC = TextEditingController();
  TextEditingController passwordBaruC = TextEditingController();
  TextEditingController passwordBaruKonfirmasiC = TextEditingController();
  TextEditingController emailC = TextEditingController();
  TextEditingController phoneC = TextEditingController();

  TextEditingController nameC = TextEditingController();
  TextEditingController usiaC = TextEditingController();
  TextEditingController ttlC = TextEditingController();
  TextEditingController alamatC = TextEditingController();
  RxString selectedNikahValue = RxString("0");
  final List<Nikah> itemsNikah = [
    Nikah("Belum Menikah", "0"),
    Nikah("Sudah Menikah", "1"),
  ];

  RxBool isLoading2 = RxBool(true);
  RxBool isLoading = RxBool(true);
  RxBool isLoading1 = RxBool(true);

  ApiReturnValue userUpdate =
      ApiReturnValue(value: null, message: null, statusCode: null);

  final List<String> itemsAgama = [
    'Islam',
    'Kristen Protestan',
    'Kristen Katolik',
    'Hindu',
    'Buddha',
    'Konghucu',
  ];
  RxString selectedAgamaValue = RxString('Islam');

  final List<String> items = [
    'Pria',
    'Wanita',
  ];
  RxString selectedValue = RxString('Pria');
  @override
  void onInit() {
    getData();
    super.onInit();
  }

  Future<void> getData() async {
    String? jns = await AuthServices.getJnsKelamin();
    this.nameC.text = (await AuthServices.getName())!;
    this.img = (await AuthServices.getPicture());
    this.nameC.text = (await AuthServices.getName())!;
    this.ttlC.text = (await AuthServices.getTtl())!;

    this.emailC.text = (await AuthServices.getEmail())!;
    this.phoneC.text = (await AuthServices.getPhone())!;
    this.alamatC.text = (await AuthServices.getAlamat())!;
    this.selectedAgamaValue.value = (await AuthServices.getAgama()) ?? "Islam";
    this.selectedValue.value = await AuthServices.getJnsKelamin() ?? "Wanita";
    print(await AuthServices.getNikah());
    this.selectedNikahValue.value = await AuthServices.getNikah() ?? "0";

    this.phoneC.text = (await AuthServices.getPhone())!;
    // this.items = [
    //   jns ?? "Wanita",
    //   'Pria',
    //   'Wanita',
    // ];
    print(items);
    Future.delayed(const Duration(milliseconds: 1500), () {
      isLoading.value = true;
    });
  }

  void pickImage() async {
    print("image");
    isLoading2.value = false;

    PickedFile? image1 = await picker.getImage(source: ImageSource.gallery);
    if (image1 != null) {
      image = File(image1.path);
      isLoading2.value = true;
      update();
    }
  }

  void updateProfile(UpdateProfileRequest request) async {
    try {
      isLoading1.value = true;
      ApiReturnValue category1 =
          await UserServices.updateProfile(request) as ApiReturnValue;
      userUpdate = category1;
      print(userUpdate);
    } finally {
      isLoading1.value = false;
    }
  }
}
