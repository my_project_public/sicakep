import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/models/models.dart';

class RegisterController extends GetxController {
  RxBool obsecureText = RxBool(true);
  TextEditingController emailC = TextEditingController();
  TextEditingController phoneC = TextEditingController();
  TextEditingController passwordC = TextEditingController();
  TextEditingController passwordConfirmC = TextEditingController();
  RegisterRequest registerRequest = new RegisterRequest();

   @override
    void onClose() {
      emailC.text = '';
      phoneC.text = '';
      passwordC.text = '';
      passwordConfirmC.text = '';
      super.onClose();
    }

}
