import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/alert/custom_alert.dart';
import 'package:ppa_lapor/app/widgets/custom_input.dart';

import '../controllers/register_controller.dart';

class RegisterView extends GetView<RegisterController> {
  const RegisterView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        title: Text(
          'Daftar',
          style: TextStyle(
            fontFamily: 'inter',
            fontSize: 26,
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border: Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 24),
        children: [
          SizedBox(height: 32),
          Text(
            'Masukkan info untuk melanjutkan',
            style: TextStyle(
              color: AppColor.grey,
              fontFamily: 'inter',
              fontWeight: FontWeight.w700,
            ),
          ),
          CustomInput(
            controller: controller.emailC,
            label: 'Email*',
            hint: 'Masukkan Email',
            cek: false,
              cekArray: false,
            prefixIcon: Icon(Icons.mail, color: AppColor.grey),
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),
          CustomInput(
            controller: controller.phoneC,
            label: 'No HP*',
            hint: '08XXXXXXXXXX',
            cek: false,
            cekArray: false,
            prefixIcon: Icon(Icons.phone, color: AppColor.grey),
            margin: EdgeInsets.only(bottom: 16),
          ),
          Obx(
            () => CustomInput(
              controller: controller.passwordC,
              label: 'Password*',
              hint: '***********',
              cek: false,
              cekArray: false,
              obsecureText: controller.obsecureText.value,
              prefixIcon: Container(
                padding: EdgeInsets.all(16),
                child: SvgPicture.asset('assets/icons/Lock.svg', color: AppColor.grey),
              ),
              suffixIcon: IconButton(
                icon: (controller.obsecureText.value != false)
                    ? Icon(
                        Icons.visibility,
                        color: AppColor.grey,
                      )
                    : Icon(
                        Icons.visibility_off,
                        color: AppColor.grey,
                      ),
                onPressed: () {
                  controller.obsecureText.value = !(controller.obsecureText.value);
                },
                color: AppColor.grey,
              ),
              margin: EdgeInsets.only(bottom: 16),
            ),
          ),
          Obx(
            () => CustomInput(
              controller: controller.passwordConfirmC,
              label: 'Konfirmasi Password*',
              hint: '***********',
              cek: false,
              cekArray: false,
              obsecureText: controller.obsecureText.value,
              prefixIcon: Container(
                padding: EdgeInsets.all(16),
                child: SvgPicture.asset('assets/icons/Lock.svg', color: AppColor.grey),
              ),
              suffixIcon: IconButton(
                icon: (controller.obsecureText.value != false)
                    ? Icon(
                        Icons.visibility,
                        color: AppColor.grey,
                      )
                    : Icon(
                        Icons.visibility_off,
                        color: AppColor.grey,
                      ),
                onPressed: () {
                  controller.obsecureText.value = !(controller.obsecureText.value);
                },
                color: AppColor.grey,
              ),
              margin: EdgeInsets.only(bottom: 24),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 48,
            margin: EdgeInsets.only(top: 8),
            child: ElevatedButton(
              child: Text(
                'Next',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, fontFamily: 'inter'),
              ),
              onPressed: () {
                if (!EmailValidator.validate(controller.emailC.text)) {
                  CustomAlert.show(
                    title: 'Email',
                    cek: false,
                    description: "Email Tidak boleh kosong!",
                    onTap: () {},
                  );
                  
                }
                if(controller.phoneC.text.length < 7){
                  CustomAlert.show(
                    title: 'Nomor Hp',
                    cek: false,
                    description: "Nomor Hp minimal 8!",
                    onTap: () {},
                  );
                }
                if(controller.passwordC.text.length < 7){
                  CustomAlert.show(
                    title: 'Password',
                    cek: false,
                    description: "password  minimal 8 karakter!",
                    onTap: () {},
                  );
                }
                if(controller.passwordC.text != controller.passwordConfirmC.text){
                  CustomAlert.show(
                    title: 'Password Konfirmasi',
                    cek: false,
                    description: "Password Konfirmasi tidak sesuai!",
                    onTap: () {},
                  );
                }
                controller.registerRequest.email = controller.emailC.text;
                controller.registerRequest.phone = controller.phoneC.text;
                controller.registerRequest.password = controller.passwordC.text;
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) =>  RegisterNextPage(email: _email, password: _password,)),
                // );
                Get.toNamed(Routes.REGISTERNEXT, arguments: controller.registerRequest);
              },
              style: ElevatedButton.styleFrom(
                primary: AppColor.primaryColor,
                elevation: 0,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
              ),
            ),
          ), 

          // Login
          Container(
            margin: EdgeInsets.only(top: 32),
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Sudah memiliki akun?',
                  style: TextStyle(
                    fontFamily: 'inter',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Get.toNamed(Routes.LOGIN);
                  },
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontFamily: 'inter',
                      fontWeight: FontWeight.w600,
                      color: AppColor.primaryColor,
                    ),
                  ),
                  style: TextButton.styleFrom(
                    primary: AppColor.primaryColor,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
