import 'dart:io';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/modules/korban/controllers/korban_controller.dart';
import 'package:ppa_lapor/app/modules/saksi_mata/controllers/saksi_mata_controller.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/alert/custom_alert.dart';
import 'package:ppa_lapor/app/widgets/custom_input.dart';
import 'package:ppa_lapor/models/models.dart';

import '../controllers/lapor_kekerasan_controller.dart';

class LaporKekerasanView extends GetView<LaporKekerasanController> {
  LaporKekerasanView({Key? key}) : super(key: key);
  File? image;

  @override
  Widget build(BuildContext context) {
    // if (Get.arguments != null) {
    //   controller.korbanRequest = Get.arguments;
    // }
    return Scaffold(
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        child: Obx(
          () => Container(
            width: MediaQuery.of(context).size.width,
            height: 48,
            margin: EdgeInsets.only(top: 8),
            child: ElevatedButton(
              child: (controller.isLoading1.value == true)
                  ? Text(
                      'Loading...',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'inter'),
                    )
                  : Text(
                      'Simpan',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'inter'),
                    ),
              onPressed: () {
                if (controller.isLoading1.value == true) {
                  return;
                }
                controller.isLoading1.value = true;

                if (controller.selectedValue.value.length < 1) {
                  CustomAlert.show(
                    title: 'Jenis Kekerasan',
                    cek: true,
                    description: "Jenis Kekerasan Tidak Boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }
                if (controller.kronologisC.text.length < 1) {
                  CustomAlert.show(
                    title: 'Krnologi',
                    cek: true,
                    description: "Krnologi tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }
                if (controller.tanggalC.text.length < 1) {
                  CustomAlert.show(
                    title: 'Tanggal Kejadian',
                    cek: true,
                    description: "Tanggal Kejadian tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }
                if (controller.alamatC.text.length < 1) {
                  CustomAlert.show(
                    title: 'Alamat Kejadian',
                    cek: true,
                    description: "Alamat Kejadian tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }

                controller.korbanRequest.jenisKekerasanId =
                    controller.selectedValue.value;
                controller.korbanRequest.tglKejadian = controller.tanggalC.text;
                controller.korbanRequest.kronologi =
                    controller.kronologisC.text;
                controller.korbanRequest.alamatK = controller.alamatC.text;
                controller.korbanRequest.image = controller.image;
                print(controller.korbanRequest);
                if (controller.korbanRequest.jenis == 1) {
                  controller.storeLaporanSaksi(controller.korbanRequest);
                } else {
                  controller.storeLaporanKorban(controller.korbanRequest);
                }

                Future.delayed(Duration(milliseconds: 3000), () {
                  print(controller.laporan.statusCode);
                  if (controller.isLoading1.value == false) {
                    print(controller.laporan.statusCode);
                    if (controller.laporan.statusCode == 200) {
                      CustomAlert.show(
                        title: 'Laporan Success',
                        description: "Laporan berhasil dikirim!",
                        onTap: () {
                          Get.delete<LaporKekerasanController>();
                          Get.delete<SaksiMataController>();
                          Get.delete<KorbanController>();
                          Get.toNamed(Routes.HOME);
                        },
                      );
                    } else {
                      CustomAlert.show(
                        title: 'Laporan Gagal',
                        cek: true,
                        description: controller.laporan.message,
                        onTap: () {},
                      );
                      return;
                    }
                  } else {}
                });
              },
              style: ElevatedButton.styleFrom(
                primary: AppColor.primaryColor,
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)),
              ),
            ),
          ),
        ),
      ),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        title: Text(
          'Lapor Kekerasan ' + controller.argument[0],
          style: TextStyle(
            fontSize: 24,
            color: Colors.black,
            fontWeight: FontWeight.w600,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border:
                  Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 24),
        children: [
          SizedBox(height: 32),
          Text(
            'Keterangan Tambahan*',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              fontFamily: 'inter',
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16, bottom: 4),
            child: Text(
              'Jenis Kekerasan*',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w500,
                fontFamily: 'inter',
              ),
            ),
          ),
          Obx(
            () => (controller.isLoading.value)
                ? Center(child: CircularProgressIndicator())
                : (controller.category.value == null)
                    ? Center(child: CircularProgressIndicator())
                    : SizedBox(
                        height: 48,
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                            buttonDecoration: BoxDecoration(
                              border:
                                  Border.all(color: AppColor.grey, width: 1),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            hint: Text(
                              'Select Item',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Theme.of(context).hintColor),
                            ),
                            items: controller.category.value
                                .map<DropdownMenuItem<String>>(
                                    (item) => new DropdownMenuItem<String>(
                                          value: item.id.toString(),
                                          child: Text(item.judul),
                                        ))
                                .toList(),
                            value: controller.selectedValue.value,
                            onChanged: (value) {
                              controller.selectedValue.value = value as String;
                            },
                            buttonHeight: 40,
                            buttonWidth: 140,
                            itemHeight: 40,
                          ),
                        ),
                      ),
          ),
          CustomInput(
            cek: false,
            cekArray: false,
            controller: controller.kronologisC,
            maxLines: null,
            label: 'Kronologis Kasus*',
            hint: 'Masukkan kronologis',
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),
          CustomInput(
            cek: true,
            cekArray: false,
            controller: controller.tanggalC,
            maxLines: 1,
            label: 'Tanggal Kejadian*',
            hint: 'dd/mm/yy',
            margin: EdgeInsets.only(bottom: 16),
          ),
          CustomInput(
            maxLines: null,
            controller: controller.alamatC,
            label: 'Alamat kejadian*',
            hint: 'Masukkan Alamat',
            cek: false,
            cekArray: false,
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),
          Obx(() => (!controller.isLoading2.value)
              ? InkWell(
                  onTap: () {
                    _showChoiceDialog(context);
                    // controller.pickImage();
                  },
                  borderRadius: BorderRadius.circular(12),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 200,
                    decoration: BoxDecoration(
                      border:
                          Border.all(color: AppColor.primaryColor, width: 1),
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.transparent,
                    ),
                    alignment: Alignment.center,
                    child: (controller.image == null)
                        ? Text(
                            'Upload Foto Visum',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: AppColor.primaryColor,
                            ),
                          )
                        : Container(
                            child: Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: FileImage(controller.image!),
                                        fit: BoxFit.cover)))),
                  ))
              : Center(child: CircularProgressIndicator()))
        ],
      ),
    );
  }

  void _openGallery(BuildContext context) async {
    controller.pickImage();

    Navigator.pop(context);
  }

  void _openCamera(BuildContext context) async {
    controller.pickImageCamera();

    Navigator.pop(context);
  }

  Future<void> _showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Choose option",
              style: TextStyle(color: Colors.blue),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                    },
                    title: Text("Gallery"),
                    leading: Icon(
                      Icons.account_box,
                      color: Colors.blue,
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                    },
                    title: Text("Camera"),
                    leading: Icon(
                      Icons.camera,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
