import 'package:get/get.dart';

import '../controllers/lapor_kekerasan_controller.dart';

class LaporKekerasanBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LaporKekerasanController>(
      () => LaporKekerasanController(),
    );
  }
}
