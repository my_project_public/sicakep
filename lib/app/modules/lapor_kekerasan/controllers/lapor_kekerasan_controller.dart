import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/models/models.dart';
import 'package:ppa_lapor/services/services.dart';
import 'package:image_picker/image_picker.dart';

class LaporKekerasanController extends GetxController {
  TextEditingController kronologisC = TextEditingController();
  TextEditingController tanggalC = TextEditingController();
  TextEditingController alamatC = TextEditingController();
  RxString selectedValue = RxString("1");
  String title = "";
  KorbanRequest korbanRequest = new KorbanRequest();
  List argument = json.decode(Get.arguments);

  ApiReturnValue category =
      ApiReturnValue(value: null, message: null, statusCode: null);
  ApiReturnValue laporan =
      ApiReturnValue(value: null, message: null, statusCode: null);
  @override
  void onInit() {
    List argument1 = json.decode(Get.arguments);

    if (Get.arguments != null) {
      print(argument1[1]);
      if (argument1[1] != 1) {
        korbanRequest = argument1[1];
      }
      title = argument1[0];
    }

    getCategory();
    super.onInit();
  }

  final List<String> items = [
    'Fisik',
    'Psikis',
    'Seksual',
  ];

  RxBool isLoading1 = RxBool(false);
  RxBool isLoading = RxBool(false);
  RxBool isLoading2 = RxBool(false);

  @override
  void onClose() {
    kronologisC.text = '';
    tanggalC.text = '';
    image = null;

    korbanRequest = new KorbanRequest();
    super.onClose();
  }

  void getCategory() async {
    try {
      isLoading.value = true;
      ApiReturnValue category1 =
          await CategoryServices.getCategory() as ApiReturnValue;
      category = category1;
    } finally {
      isLoading.value = false;
    }
  }

  void storeLaporanKorban(KorbanRequest request) async {
    try {
      isLoading1.value = true;
      ApiReturnValue category1 =
          await LaporanServices.storeKorban(request) as ApiReturnValue;
      laporan = category1;
    } finally {
      isLoading1.value = false;
    }
  }

  void storeLaporanSaksi(KorbanRequest request) async {
    try {
      isLoading1.value = true;
      ApiReturnValue category1 =
          await LaporanServices.storeSaksi(request) as ApiReturnValue;
      laporan = category1;
    } finally {
      isLoading1.value = false;
    }
  }

  // RxString selectedValue = RxString('Fisik');

  final ImagePicker picker = ImagePicker();
  File? image;

  void pickImage() async {
    isLoading2.value = true;
    PickedFile? image1 = await picker.getImage(source: ImageSource.gallery);
    if (image1 != null) {
      image = File(image1.path);
      isLoading2.value = false;
      update();
    }
  }

  void pickImageCamera() async {
    isLoading2.value = true;
    PickedFile? image1 = await picker.getImage(source: ImageSource.camera);
    if (image1 != null) {
      image = File(image1.path);
      isLoading2.value = false;
      update();
    }
  }
}
