import 'dart:convert';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/custom_input.dart';

import '../../../widgets/alert/custom_alert.dart';
import '../controllers/korban_controller.dart';

class KorbanView extends GetView<KorbanController> {
  const KorbanView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (Get.arguments != null) {
      controller.korbanRequest = Get.arguments;
    }

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        title: Text(
          'Lapor Kekerasan Saksi',
          style: TextStyle(
            fontSize: 24,
            color: Colors.black,
            fontWeight: FontWeight.w600,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border:
                  Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 32),
        children: [
          Text(
            'Identitas Korban*',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              fontFamily: 'inter',
            ),
          ),
          CustomInput(
            cek: false,
            cekArray: false,
            controller: controller.nameC,
            label: 'Nama Korban*',
            hint: 'Masukkan nama korban',
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),

          CustomInput(
            cek: true,
            cekArray: false,
            controller: controller.ttlC,
            label: 'TTL Korban*',
            hint: 'Masukkan tempat tanggal lahir korban',
            margin: EdgeInsets.only(bottom: 16),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 4),
            child: Text(
              'Jenis Kelamin Korban*',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w500,
                fontFamily: 'inter',
              ),
            ),
          ),
          // Jenis Kelamin
          Obx(
            () => SizedBox(
              height: 48,
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                  buttonDecoration: BoxDecoration(
                    border: Border.all(color: AppColor.grey, width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hint: Text(
                    'Select Item',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).hintColor),
                  ),
                  items: controller.kelaminItems
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(item),
                          ))
                      .toList(),
                  value: controller.kelaminSelectedValue.value,
                  onChanged: (value) {
                    controller.kelaminSelectedValue.value = value as String;
                  },
                  buttonHeight: 40,
                  buttonWidth: 140,
                  itemHeight: 40,
                ),
              ),
            ),
          ),
          // Agama
          Container(
            margin: EdgeInsets.only(bottom: 4, top: 16),
            child: Text(
              'Agama Korban',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w500,
                fontFamily: 'inter',
              ),
            ),
          ),
          Obx(
            () => SizedBox(
              height: 48,
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                  buttonDecoration: BoxDecoration(
                    border: Border.all(color: AppColor.grey, width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hint: Text(
                    'Select Item',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).hintColor),
                  ),
                  items: controller.agamaItems
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(item),
                          ))
                      .toList(),
                  value: controller.agamaSelectedValue.value,
                  onChanged: (value) {
                    controller.agamaSelectedValue.value = value as String;
                  },
                  buttonHeight: 40,
                  buttonWidth: 140,
                  itemHeight: 40,
                ),
              ),
            ),
          ),
          Text(
            'Korban Sudah Menikah?',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w500,
              fontFamily: 'inter',
            ),
          ),
          SizedBox(height: 4),

          Obx(
            () => SizedBox(
              height: 48,
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                  buttonDecoration: BoxDecoration(
                    border: Border.all(color: AppColor.grey, width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hint: Text(
                    'Status Nikah',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).hintColor),
                  ),
                  items: controller.itemsNikah
                      .map((item) => DropdownMenuItem<String>(
                            value: item.id,
                            child: Text(item.name),
                          ))
                      .toList(),
                  value: controller.selectedNikahValue.value,
                  onChanged: (value) {
                    controller.selectedNikahValue.value = value as String;
                  },
                  buttonHeight: 40,
                  buttonWidth: 140,
                  itemHeight: 40,
                ),
              ),
            ),
          ),

          //hubungan

          Text(
            'Hubungan korban dengan pelaku?',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w500,
              fontFamily: 'inter',
            ),
          ),
          SizedBox(height: 4),

          Obx(
            () => SizedBox(
              height: 48,
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                  buttonDecoration: BoxDecoration(
                    border: Border.all(color: AppColor.grey, width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hint: Text(
                    'Hubungan',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).hintColor),
                  ),
                  items: controller.hubunganItems
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(item),
                          ))
                      .toList(),
                  value: controller.hubunganC.value,
                  onChanged: (value) {
                    controller.hubunganC.value = value as String;
                  },
                  buttonHeight: 40,
                  buttonWidth: 140,
                  itemHeight: 40,
                ),
              ),
            ),
          ),
          // CustomInput(
          //   cek: false,
          //   cekArray: false,
          //   controller: controller.hubunganC,
          //   maxLines: null,
          //   label: 'Hubungan dengan pelaku*',
          //   hint: 'Masukkan hubungan dengan korban',
          //   margin: EdgeInsets.only(top: 16),
          // ),
          CustomInput(
            cek: false,
            cekArray: false,
            controller: controller.alamatC,
            maxLines: null,
            label: 'Alamat korban*',
            hint: 'Masukkan alamat korban',
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),
          CustomInput(
            cek: false,
            cekArray: false,
            controller: controller.nohpC,
            label: 'No HP korban',
            hint: '08XXXXXXXXXX',
            textInputType: TextInputType.number,
            margin: EdgeInsets.only(bottom: 16),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 48,
            margin: EdgeInsets.only(top: 8),
            child: ElevatedButton(
              child: Text(
                'Selanjutnya',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'inter'),
              ),
              onPressed: () {
                if (controller.nameC.text.length < 1) {
                  CustomAlert.show(
                    title: 'Nama',
                    cek: false,
                    description: "Nama tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }

                if (controller.ttlC.text.length < 1) {
                  CustomAlert.show(
                    title: 'Tanggal Lahir',
                    cek: false,
                    description: "Tanggal Lahir tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }
                // if (controller.nohpC.text.length < 1) {
                //   CustomAlert.show(
                //     title: 'Nomor Hp',
                //     cek: false,
                //     description: "Nomor Hp tidak boleh kosong!",
                //     onTap: () {},
                //   );
                //   return;
                // }
                if (controller.alamatC.text.length < 1) {
                  CustomAlert.show(
                    title: 'Alamat',
                    cek: false,
                    description: "Alamat tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }
                // if (controller.hubunganC.value.length < 1) {
                //   CustomAlert.show(
                //     title: 'Hubungan',
                //     cek: false,
                //     description: "Hubungan tidak boleh kosong!",
                //     onTap: () {},
                //   );
                //   return;
                // }
                if (controller.kelaminSelectedValue.value.length < 1) {
                  CustomAlert.show(
                    title: 'Jenis Kelamin',
                    cek: false,
                    description: "Jenis Kelamin tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }
                // if (controller.agamaSelectedValue.value.length < 1) {
                //   CustomAlert.show(
                //     title: 'Agama',
                //     cek: false,
                //     description: "Agama tidak boleh kosong!",
                //     onTap: () {},
                //   );
                //   return;
                // }
                controller.korbanRequest.jenis = 1;

                controller.korbanRequest.namaK = controller.nameC.text;
                controller.korbanRequest.ttlK = controller.ttlC.text;
                controller.korbanRequest.alamatKo = controller.alamatC.text;
                controller.korbanRequest.phoneK = controller.nohpC.text;
                controller.korbanRequest.hub = controller.hubunganC.value;
                controller.korbanRequest.agamaK =
                    controller.agamaSelectedValue.value;
                controller.korbanRequest.genderK =
                    controller.kelaminSelectedValue.value;
                controller.korbanRequest.nikahK =
                    controller.selectedNikahValue.value;

                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) =>  RegisterNextPage(email: _email, password: _password,)),
                // );
                List temp1 = ["Saksi", controller.korbanRequest];
                String temp = json.encode(temp1);
                Get.toNamed(Routes.LAPOR_KEKERASAN, arguments: temp);
              },
              style: ElevatedButton.styleFrom(
                primary: AppColor.primaryColor,
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
