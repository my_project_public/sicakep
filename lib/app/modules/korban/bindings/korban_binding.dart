import 'package:get/get.dart';

import '../controllers/korban_controller.dart';

class KorbanBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<KorbanController>(
      () => KorbanController(),
    );
  }
}
