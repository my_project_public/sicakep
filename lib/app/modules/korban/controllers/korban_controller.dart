import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/models/models.dart';

class KorbanController extends GetxController {
  TextEditingController nameC = TextEditingController();
  TextEditingController ttlC = TextEditingController();
  TextEditingController alamatC = TextEditingController();
  // TextEditingController hubunganC = TextEditingController();
  TextEditingController nohpC = TextEditingController();
  KorbanRequest korbanRequest = new KorbanRequest();
  RxString selectedNikahValue = RxString("-1");
  final List<Nikah> itemsNikah = [
    Nikah("Pilih Status Menikah", "-1"),
    Nikah("Belum Menikah", "0"),
    Nikah("Sudah Menikah", "1"),
  ];

  final List<String> kelaminItems = [
    'Pria',
    'Wanita',
  ];
  final List<String> hubunganItems = [
    '-',
    'Istri',
    'Suami',
    'Keluarga',
    'Pacar',
    'Lainnya',
  ];
  RxString kelaminSelectedValue = RxString('Pria');
  RxString hubunganC = RxString('-');

  final List<String> agamaItems = [
    '-',
    'Islam',
    'Kristen Protestan',
    'Kristen Katolik',
    'Hindu',
    'Buddha',
    'Konghucu',
  ];
  RxString agamaSelectedValue = RxString('-');
}
