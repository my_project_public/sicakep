import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/alert/custom_alert.dart';
import 'package:ppa_lapor/app/widgets/custom_input.dart';

import '../controllers/reset_password_controller.dart';

class ResetPasswordView extends GetView<ResetPasswordController> {
  const ResetPasswordView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border: Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        children: [
          // Welcome Title
          Container(
            margin: EdgeInsets.only(bottom: 32),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Setel Ulang Password',
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 26,
                    fontFamily: 'inter',
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  'Masukkan nomor Ponsel Anda untuk mendapatkan reset kode pada Nomor Ponsel Anda.',
                  style: TextStyle(
                    color: Color(0xFF727F8D),
                    fontFamily: 'inter',
                  ),
                ),
              ],
            ),
          ),
          // Form
          // Password Lama
          Obx(
            () => CustomInput(
              cek: false,
              cekArray: false,
              controller: controller.passwordC,
              label: 'Password Baru*',
              hint: '***********',
              obsecureText: controller.obsecureText.value,
              prefixIcon: Container(
                padding: EdgeInsets.all(16),
                child: SvgPicture.asset('assets/icons/Lock.svg', color: AppColor.grey),
              ),
              suffixIcon: IconButton(
                icon: (controller.obsecureText.value != false)
                    ? Icon(
                        Icons.visibility,
                        color: AppColor.grey,
                      )
                    : Icon(
                        Icons.visibility_off,
                        color: AppColor.grey,
                      ),
                onPressed: () {
                  controller.obsecureText.value = !(controller.obsecureText.value);
                },
                color: AppColor.grey,
              ),
              margin: EdgeInsets.only(bottom: 16),
            ),
          ),
          Obx(
            () => CustomInput(
              cek: false,
              cekArray: false,
              controller: controller.passwordConfirmC,
              label: 'Konfirmasi Password*',
              hint: '***********',
              obsecureText: controller.obsecureText.value,
              prefixIcon: Container(
                padding: EdgeInsets.all(16),
                child: SvgPicture.asset('assets/icons/Lock.svg', color: AppColor.grey),
              ),
              suffixIcon: IconButton(
                icon: (controller.obsecureText.value != false)
                    ? Icon(
                        Icons.visibility,
                        color: AppColor.grey,
                      )
                    : Icon(
                        Icons.visibility_off,
                        color: AppColor.grey,
                      ),
                onPressed: () {
                  controller.obsecureText.value = !(controller.obsecureText.value);
                },
                color: AppColor.grey,
              ),
              margin: EdgeInsets.only(bottom: 24),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 48,
            child: ElevatedButton(
              child: Text(
                'Setel Ulang Password',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, fontFamily: 'inter'),
              ),
              onPressed: () {
                CustomAlert.show(
                  title: 'Selamat!',
                  description: 'Setel ulang password Anda telah berhasil dilakukan.',
                  onTap: () {
                    Get.toNamed(Routes.HOME);
                  },
                );
              },
              style: ElevatedButton.styleFrom(
                primary: AppColor.primaryColor,
                elevation: 0,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
              ),
            ),
          )
        ],
      ),
    );
  }
}
