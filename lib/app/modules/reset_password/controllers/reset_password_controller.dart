import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ResetPasswordController extends GetxController {
  RxBool obsecureText = RxBool(true);

  TextEditingController passwordC = TextEditingController();
  TextEditingController passwordConfirmC = TextEditingController();
}
