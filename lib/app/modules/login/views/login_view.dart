import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/alert/custom_alert.dart';
import 'package:ppa_lapor/app/widgets/custom_input.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        title: Text(
          'Selamat Datang',
          style: TextStyle(
            fontFamily: 'inter',
            fontSize: 26,
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border: Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 24),
        children: [
          SizedBox(height: 32),
          CustomInput(
            controller: controller.emailC,
            label: 'Email*',
            hint: 'Masukkan Email',
            cek: false,
            cekArray: false,
            prefixIcon: Icon(Icons.person, color: AppColor.grey),
            margin: EdgeInsets.only(bottom: 16),
          ),
          Obx(
            () => CustomInput(
              controller: controller.passwordC,
              label: 'Password*',
              hint: '***********',
              cek: false,
              cekArray: false,
              obsecureText: controller.obsecureText.value,
              prefixIcon: Container(
                padding: EdgeInsets.all(16),
                child: SvgPicture.asset('assets/icons/Lock.svg', color: AppColor.grey),
              ),
              suffixIcon: IconButton(
                icon: (controller.obsecureText.value != false)
                    ? Icon(
                        Icons.visibility,
                        color: AppColor.grey,
                      )
                    : Icon(
                        Icons.visibility_off,
                        color: AppColor.grey,
                      ),
                onPressed: () {
                  controller.obsecureText.value = !(controller.obsecureText.value);
                },
                color: AppColor.grey,
              ),
              margin: EdgeInsets.only(bottom: 0),
            ),
          ),
          TextButton(
            onPressed: () {
              Get.toNamed(Routes.FORGET_PASSWORD);
            },
            child: Text('Lupa Password'),
            style: TextButton.styleFrom(
              primary: AppColor.primaryColor,
              alignment: Alignment.centerRight,
            ),
          ),
          Obx(
            () =>
            Container(
              width: MediaQuery.of(context).size.width,
              height: 48,
              margin: EdgeInsets.only(top: 8),
              child: ElevatedButton(
                child: (controller.isLoading.value == true) ? Text(
                'Loading...',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, fontFamily: 'inter'),
              ) :Text(
                  'Masuk',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, fontFamily: 'inter'),
                ),
                onPressed: () {
                  if(controller.isLoading.value == true){
                    return;
                  }
                   if (!EmailValidator.validate(controller.emailC.text)) {
                    CustomAlert.show(
                      title: 'Email',
                      cek: false,
                      description: "Email Tidak boleh kosong!",
                      onTap: () {},
                    );
                  
                  }
                if(controller.passwordC.text.length < 7){
                  CustomAlert.show(
                    title: 'Password',
                    cek: false,
                    description: "password  minimal 8 karakter!",
                    onTap: () {},
                  );
                }
                controller.loginRequest.email = controller.emailC.text;
                controller.loginRequest.password = controller.passwordC.text;
              

                controller.login(controller.loginRequest);
                Future.delayed(Duration(milliseconds: 5000), () {
                  print("code");
                  print(controller.user.statusCode);
                  if(controller.isLoading.value == false){
                    print(controller.user.statusCode);
                    if(controller.user.statusCode == 200){
                      Get.toNamed(Routes.HOME);
                    }else{
                      CustomAlert.show(
                        cek: true,
                        title: 'Register Gagal',
                        description: "Akun Tidak ditemukan!",
                        onTap: () {},
                      );
                    }
                  }else{
                  }
                });
                  // Get.toNamed(Routes.HOME);
                },
                style: ElevatedButton.styleFrom(
                  primary: AppColor.primaryColor,
                  elevation: 0,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                ),
              ),
            ),
          ),
          // Container(
          //   margin: EdgeInsets.only(top: 24, bottom: 24),
          //   width: MediaQuery.of(context).size.width,
          //   padding: EdgeInsets.symmetric(horizontal: 42),
          //   child: Row(
          //     children: [
          //       Expanded(child: Divider(color: AppColor.grey, thickness: 2)),
          //       Container(
          //         margin: EdgeInsets.symmetric(horizontal: 16),
          //         child: Text('Atau login dengan'),
          //       ),
          //       Expanded(child: Divider(color: AppColor.grey, thickness: 2)),
          //     ],
          //   ),
          // ),
          // Social media login
          // Container(
          //   width: MediaQuery.of(context).size.width,
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     children: [
          //       // Facebook
          //       // Container(
          //       //   width: 140,
          //       //   height: 48,
          //       //   margin: EdgeInsets.only(top: 8, right: 16),
          //       //   child: ElevatedButton(
          //       //     child: Row(
          //       //       mainAxisAlignment: MainAxisAlignment.center,
          //       //       children: [
          //       //         SvgPicture.asset(
          //       //           'assets/icons/facebook.svg',
          //       //           color: Colors.white,
          //       //         ),
          //       //         SizedBox(width: 8),
          //       //         Text(
          //       //           'Facebook',
          //       //           style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600, fontFamily: 'inter'),
          //       //         ),
          //       //       ],
          //       //     ),
          //       //     onPressed: () {},
          //       //     style: ElevatedButton.styleFrom(
          //       //       primary: Color(0xFF3402FD),
          //       //       elevation: 0,
          //       //       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          //       //     ),
          //       //   ),
          //       // ),
          //       // Google
          //       // Container(
          //       //   width: 140,
          //       //   height: 48,
          //       //   margin: EdgeInsets.only(top: 8),
          //       //   child: ElevatedButton(
          //       //     child: Row(
          //       //       mainAxisAlignment: MainAxisAlignment.center,
          //       //       children: [
          //       //         SvgPicture.asset(
          //       //           'assets/icons/google.svg',
          //       //           color: Colors.white,
          //       //         ),
          //       //         SizedBox(width: 8),
          //       //         Text(
          //       //           'Google',
          //       //           style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600, fontFamily: 'inter'),
          //       //         ),
          //       //       ],
          //       //     ),
          //       //     onPressed: () {},
          //       //     style: ElevatedButton.styleFrom(
          //       //       primary: Color(0xFFFD0202),
          //       //       elevation: 0,
          //       //       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          //       //     ),
          //       //   ),
          //       // ),
          //     ],
          //   ),
          // ),

          // Daftar
          Container(
            margin: EdgeInsets.only(top: 32),
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Tidak memiliki akun?',
                  style: TextStyle(
                    fontFamily: 'inter',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Get.toNamed(Routes.REGISTER);
                  },
                  child: Text(
                    'Daftar',
                    style: TextStyle(
                      fontFamily: 'inter',
                      fontWeight: FontWeight.w600,
                      color: AppColor.primaryColor,
                    ),
                  ),
                  style: TextButton.styleFrom(
                    primary: AppColor.primaryColor,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
