import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/models/models.dart';
import 'package:ppa_lapor/services/services.dart';

class LoginController extends GetxController {
  RxBool obsecureText = RxBool(true);
  TextEditingController emailC = TextEditingController();
  TextEditingController passwordC = TextEditingController();

  LoginRequest loginRequest = new LoginRequest();
  ApiReturnValue user =
      ApiReturnValue(value: null, message: null, statusCode: null);

  RxBool isLoading = RxBool(false);
  @override
  void onClose() {
    emailC.text = '';
    passwordC.text = '';
    super.onClose();
  }

  void login(LoginRequest request) async {
    try {
      isLoading.value = true;
      ApiReturnValue user1 =
          await AuthServices.login(request) as ApiReturnValue;
      // ignore: unnecessary_null_comparison
      // if (user1.value != null || user1.message != null) {
      // ignore: unnecessary_cast
      user = user1;
      print(user);
      // }
    } finally {
      isLoading.value = false;
    }
  }
}
