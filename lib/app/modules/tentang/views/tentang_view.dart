import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/style/app_color.dart';

import '../controllers/tentang_controller.dart';

class TentangView extends GetView<TentangController> {
  const TentangView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text(
          'Tentang',
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w700,
            fontFamily: 'inter',
            color: Colors.black,
          ),
        ),
        leadingWidth: 76,
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border: Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.all(24),
        child: Container(
          padding: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: AppColor.grey.withOpacity(0.1),
            borderRadius: BorderRadius.circular(18),
          ),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 32, bottom: 24),
                child: Text(
                  'UPTD PPA',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w700,
                    fontFamily: 'inter',
                  ),
                ),
              ),
              Text(
                'UPTD PPA ini adalah bentuk komitmen Pemerintah Provinsi NTB dalam mewujudkan perempuan yang berdaya dan melahirkan anak-anak yang berkualitas. Inilah bentuk nyata keseriusan pemerintah daerah dalam memberikan perhatian terhadap perlindungan perempuan dan masa depan anak-anak di NTB.\n\nTempat ini akan menjadi wadah untuk konsultasi, memberikan pendidikan dan sosialisasi, pelatihan dan bahkan advokasi kepada perempuan dan anak',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                  fontFamily: 'inter',
                  color: Colors.black.withOpacity(0.5),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
