import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/alert/custom_alert.dart';
import 'package:ppa_lapor/app/widgets/main_bottom_navbar.dart';

import '../controllers/pengaturan_controller.dart';

class PengaturanView extends GetView<PengaturanController> {
  const PengaturanView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: MainBottomNavbar(),
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          centerTitle: false,
          title: Text(
            'Pengaturan',
            style: TextStyle(
              fontSize: 24,
              fontFamily: 'inter',
              color: Colors.black,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        body: Obx(
          () => (controller.isLoading.value == false)
              ? Center(child: CircularProgressIndicator())
              : ListView(
                  shrinkWrap: true,
                  children: [
                    // Profile
                    Container(
                      margin: EdgeInsets.only(top: 16),
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Row(
                        children: [
                          Container(
                            width: 64,
                            height: 64,
                            margin: EdgeInsets.only(right: 16),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: Colors.black,
                              image: DecorationImage(
                                image: NetworkImage(controller.avatar),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  controller.name,
                                  style: TextStyle(
                                    fontFamily: 'inter',
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                SizedBox(height: 2),
                                Text(
                                  controller.email,
                                  style: TextStyle(
                                    fontFamily: 'inter',
                                    color: Colors.black.withOpacity(0.7),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SettingsItem(
                      title: 'Edit Profile',
                      iconPath: 'assets/icons/Lock.svg',
                      onTap: () {
                        Get.toNamed(Routes.CHANGE_PASSWORD);
                      },
                    ),
                    // Account Settings
                    Container(
                      margin: EdgeInsets.only(top: 46),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 24),
                            child: Text(
                              'Account Settings',
                              style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'inter',
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          ListView(
                            shrinkWrap: true,
                            padding: EdgeInsets.symmetric(vertical: 16),
                            physics: BouncingScrollPhysics(),
                            children: [
                              // SettingsItem(
                              //   title: 'Change Passwords',
                              //   iconPath: 'assets/icons/Lock.svg',
                              //   onTap: () {
                              //     Get.toNamed(Routes.CHANGE_PASSWORD);
                              //   },
                              // ),
                              SettingsItem(
                                title: 'Tentang',
                                iconPath: 'assets/icons/info.svg',
                                onTap: () {
                                  Get.toNamed(Routes.TENTANG);
                                },
                              ),
                              SettingsItem(
                                title: 'Logout',
                                iconPath: 'assets/icons/logout.svg',
                                onTap: () {
                                  controller.logout();
                                  Future.delayed(Duration(milliseconds: 1000),
                                      () {
                                    print(controller.cek.statusCode);
                                    if (controller.isLoading1.value == false) {
                                      print(controller.cek.statusCode);
                                      if (controller.cek.statusCode == 200) {
                                        CustomAlert.show(
                                          title: 'Logout Berhasil',
                                          cek: false,
                                          description: controller.cek.message,
                                          onTap: () {},
                                        );
                                        Future.delayed(
                                            Duration(milliseconds: 500), () {
                                          Get.toNamed(Routes.LOGIN);
                                        });
                                      } else {
                                        CustomAlert.show(
                                          title: 'Logout Gagal',
                                          cek: true,
                                          description: controller.cek.message,
                                          onTap: () {},
                                        );
                                        return;
                                      }
                                    } else {}
                                  });
                                },
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
        ));
  }
}

class SettingsItem extends StatelessWidget {
  final String title;
  final void Function() onTap;
  final String iconPath;

  SettingsItem({
    required this.title,
    required this.onTap,
    required this.iconPath,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        width: MediaQuery.of(context).size.width,
        color: Colors.transparent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  width: 44,
                  height: 44,
                  margin: EdgeInsets.only(right: 16),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Color(0xFFDBEDFF),
                  ),
                  child: SvgPicture.asset(
                    '$iconPath',
                    color: AppColor.primaryColor,
                  ),
                ),
                Text(
                  '$title',
                  style: TextStyle(
                    fontFamily: 'inter',
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: AppColor.grey,
            ),
          ],
        ),
      ),
    );
  }
}
