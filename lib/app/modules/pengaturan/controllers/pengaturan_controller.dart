import 'package:get/get.dart';
import 'package:ppa_lapor/models/models.dart';
import 'package:ppa_lapor/services/services.dart';

class PengaturanController extends GetxController {
  final count = 0.obs;
  String name = "Budiamn";
  String email = "Budiamn";
  String avatar = "Budiamn";
  RxBool isLoading = RxBool(false);
  RxBool isLoading1 = RxBool(false);

  ApiReturnValue cek =
      ApiReturnValue(value: null, message: null, statusCode: null);

  @override
  void onInit() {
    getData();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void increment() => count.value++;
  Future<void> getData() async {
    this.name = (await AuthServices.getName())!;
    this.avatar = (await AuthServices.getPicture())!;
    this.email = (await AuthServices.getEmail())!;
    Future.delayed(const Duration(milliseconds: 500), () {
      isLoading.value = true;
    });
  }

  void logout() async {
    try {
      isLoading1.value = true;
      ApiReturnValue category1 = await AuthServices.logout() as ApiReturnValue;
      cek = category1;
    } finally {
      isLoading1.value = false;
    }
  }
}
