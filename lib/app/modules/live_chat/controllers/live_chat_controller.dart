import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/app/controllers/main_bottom_navbar_controller.dart';
import 'package:ppa_lapor/models/models.dart';
import 'package:ppa_lapor/services/services.dart';

class LiveChatController extends GetxController {
  var mainBottomNavbarController = Get.find<MainBottomNavbarController>();
  ApiReturnValue chat =
      ApiReturnValue(value: null, message: null, statusCode: null);
  TextEditingController messageC = TextEditingController();

  RxBool isLoading = RxBool(false);
  RxBool isLoadingAdd = RxBool(false);
  int id = 0;

  @override
  void onInit() {
    getData();
    getChat("0");
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> getData() async {
    this.id = (await AuthServices.getId())!;
  }

  void getChat(String skip) async {
    try {
      isLoading.value = true;
      ApiReturnValue category1 =
          await ChatServices.getChat(skip) as ApiReturnValue;
      chat = category1;
    } finally {
      isLoading.value = false;
    }
  }

  void addChat(String skip) async {
    try {
      isLoadingAdd.value = true;
      ApiReturnValue category1 =
          await ChatServices.getChat(skip) as ApiReturnValue;
      print(category1.value.length);
      print("category1");
      print(category1);
      List<Chat> items = category1.value;
      chat.value.addAll(items);
    } finally {
      Future.delayed(const Duration(milliseconds: 100), () {
        isLoadingAdd.value = false;
      });
    }
  }

  void addNewChat(String message) async {
    try {
      isLoading.value = true;
      ApiReturnValue category1 =
          await ChatServices.storeChat(message) as ApiReturnValue;
      print(category1.value);
      getChat("0");

      print(chat.value);
    } finally {
      Future.delayed(const Duration(milliseconds: 100), () {
        isLoadingAdd.value = false;
      });
    }
  }
}
