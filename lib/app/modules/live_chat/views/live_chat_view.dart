import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';

import '../controllers/live_chat_controller.dart';

import 'dart:math' as math;

class LiveChatView extends GetView<LiveChatController> {
  const LiveChatView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        centerTitle: true,
        title: Text(
          'Live Chat',
          style: TextStyle(
            fontFamily: 'inter',
            fontSize: 18,
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Get.toNamed(Routes.HOME);
            controller.mainBottomNavbarController.pageIndex.value = 0;
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border:
                  Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            Expanded(
              child: Container(
                child: Obx(() {
                  if (controller.isLoading.value) {
                    return const Center(child: CircularProgressIndicator());
                  } else {
                    return ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      physics: BouncingScrollPhysics(),
                      reverse: true,
                      padding:
                          EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                      itemBuilder: (context, index) {
                        return BubbleChat(
                          status: (controller.chat.value[index].userId ==
                                  controller.id)
                              ? 'sent'
                              : 'recived',
                          chat: controller.chat.value[index].message,
                        );
                      },
                      itemCount: controller.chat.value.length,
                    );
                  }
                }),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 70,
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
              color: Colors.transparent,
              child: TextField(
                controller:
                    controller.messageC, //editing controller of this TextField
                maxLines: null,
                decoration: InputDecoration(
                  fillColor: Color(0xFFAAB3BB).withOpacity(0.1),
                  filled: true,
                  suffixIcon: Material(
                    color: AppColor.primaryColor,
                    borderRadius: BorderRadius.circular(100),
                    child: InkWell(
                      onTap: () {
                        controller.addNewChat(controller.messageC.text);
                        controller.messageC.text = '';
                      },
                      borderRadius: BorderRadius.circular(100),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(100),
                        ),
                        child: Icon(Icons.send_rounded, color: Colors.white),
                      ),
                    ),
                  ),
                  hintText: 'Type a message here...',
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 10, horizontal: 14),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(100),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(100),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BubbleChat extends StatelessWidget {
  final String status;
  final String chat;
  BubbleChat({
    required this.status,
    required this.chat,
  });

  @override
  Widget build(BuildContext context) {
    if (status == 'sent') {
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 1.7,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12),
                    bottomLeft: Radius.circular(12),
                  ),
                  color: AppColor.primaryColor,
                ),
                child: Text(
                  '$chat',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SvgPicture.asset(
                'assets/icons/bottompath.svg',
                color: AppColor.primaryColor,
              ),
            ],
          ),
        ],
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 1.7,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                  color: AppColor.primaryColor.withOpacity(0.1),
                ),
                child: Text(
                  chat,
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Transform(
                alignment: Alignment.center,
                transform: Matrix4.rotationY(math.pi),
                child: SvgPicture.asset(
                  'assets/icons/bottompath.svg',
                  color: AppColor.primaryColor.withOpacity(0.1),
                ),
              ),
            ],
          ),
        ],
      );
    }
  }
}
