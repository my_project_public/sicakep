import 'package:get/get.dart';

import '../controllers/saksi_mata_controller.dart';

class SaksiMataBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SaksiMataController>(
      () => SaksiMataController(),
    );
  }
}
