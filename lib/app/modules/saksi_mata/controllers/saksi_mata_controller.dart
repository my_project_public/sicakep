import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/models/models.dart';

class SaksiMataController extends GetxController {
  TextEditingController nameC = TextEditingController();
  TextEditingController ttlC = TextEditingController();
  TextEditingController alamatC = TextEditingController();
  TextEditingController nohpC = TextEditingController();
  KorbanRequest korbanRequest = new KorbanRequest();
  RxString selectedNikahValue = RxString("0");
  final List<Nikah> itemsNikah = [
    Nikah("Belum Menikah", "0"),
    Nikah("Sudah Menikah", "1"),
  ];

  final List<String> items = [
    'Pria',
    'Wanita',
  ];
  RxString selectedValue = RxString('Pria');
}
