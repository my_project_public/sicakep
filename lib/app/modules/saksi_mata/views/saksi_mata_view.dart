import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/alert/custom_alert.dart';
import 'package:ppa_lapor/app/widgets/custom_input.dart';

import '../controllers/saksi_mata_controller.dart';

class SaksiMataView extends GetView<SaksiMataController> {
  const SaksiMataView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        title: Text(
          'Lapor Kekerasan Saksi',
          style: TextStyle(
            fontSize: 24,
            color: Colors.black,
            fontWeight: FontWeight.w600,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border:
                  Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 24),
        children: [
          SizedBox(height: 32),
          Text(
            'Identitas Saksi Mata*',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              fontFamily: 'inter',
            ),
          ),
          CustomInput(
            cek: false,
            cekArray: false,
            controller: controller.nameC,
            label: 'Nama*',
            hint: 'Masukkan nama anda',
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),
          CustomInput(
            cek: true,
            cekArray: false,
            controller: controller.ttlC,
            label: 'TTL*',
            hint: 'Masukkan tempat tanggal lahir anda',
            margin: EdgeInsets.only(bottom: 16),
          ),
          Text(
            'Jenis Kelamin*',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w500,
              fontFamily: 'inter',
            ),
          ),
          SizedBox(height: 4),
          Obx(
            () => SizedBox(
              height: 48,
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                  buttonDecoration: BoxDecoration(
                    border: Border.all(color: AppColor.grey, width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hint: Text(
                    'Select Item',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).hintColor),
                  ),
                  items: controller.items
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(item),
                          ))
                      .toList(),
                  value: controller.selectedValue.value,
                  onChanged: (value) {
                    controller.selectedValue.value = value as String;
                  },
                  buttonHeight: 40,
                  buttonWidth: 140,
                  itemHeight: 40,
                ),
              ),
            ),
          ),
          Text(
            'Sudah Menikah?*',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w500,
              fontFamily: 'inter',
            ),
          ),
          SizedBox(height: 4),
          Obx(
            () => SizedBox(
              height: 48,
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                  buttonDecoration: BoxDecoration(
                    border: Border.all(color: AppColor.grey, width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hint: Text(
                    'Status Nikah',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).hintColor),
                  ),
                  items: controller.itemsNikah
                      .map((item) => DropdownMenuItem<String>(
                            value: item.id,
                            child: Text(item.name),
                          ))
                      .toList(),
                  value: controller.selectedNikahValue.value,
                  onChanged: (value) {
                    controller.selectedNikahValue.value = value as String;
                  },
                  buttonHeight: 40,
                  buttonWidth: 140,
                  itemHeight: 40,
                ),
              ),
            ),
          ),
          CustomInput(
            cek: false,
            cekArray: false,
            controller: controller.alamatC,
            maxLines: null,
            label: 'Alamat*',
            hint: 'Masukkan alamat anda',
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),
          CustomInput(
            cek: false,
            cekArray: false,
            controller: controller.nohpC,
            label: 'No HP*',
            hint: '08XXXXXXXXXX',
            textInputType: TextInputType.number,
            margin: EdgeInsets.only(bottom: 16),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 48,
            margin: EdgeInsets.only(top: 8),
            child: ElevatedButton(
              child: Text(
                'Selanjutnya',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'inter'),
              ),
              onPressed: () {
                if (controller.nameC.text.length < 1) {
                  CustomAlert.show(
                    title: 'Nama',
                    cek: false,
                    description: "Nama tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }

                if (controller.ttlC.text.length < 1) {
                  CustomAlert.show(
                    title: 'Tanggal Lahir',
                    cek: false,
                    description: "Tanggal Lahir tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }
                if (controller.nohpC.text.length < 7) {
                  CustomAlert.show(
                    title: 'Nomor Hp',
                    cek: false,
                    description: "Nomor Hp tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }
                if (controller.alamatC.text.length < 1) {
                  CustomAlert.show(
                    title: 'Alamat',
                    cek: false,
                    description: "Alamat tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }
                if (controller.selectedValue.value.length < 1) {
                  CustomAlert.show(
                    title: 'Jenis Kelamin',
                    cek: false,
                    description: "Jenis Kelamin tidak boleh kosong!",
                    onTap: () {},
                  );
                  return;
                }

                controller.korbanRequest.namaS = controller.nameC.text;
                controller.korbanRequest.ttlS = controller.ttlC.text;
                controller.korbanRequest.alamatS = controller.alamatC.text;
                controller.korbanRequest.phoneS = controller.nohpC.text;
                controller.korbanRequest.genderS =
                    controller.selectedValue.value;
                controller.korbanRequest.nikahS =
                    controller.selectedNikahValue.value;
                controller.korbanRequest.jenis = 1;

                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) =>  RegisterNextPage(email: _email, password: _password,)),
                // );
                Get.toNamed(Routes.KORBAN, arguments: controller.korbanRequest);
              },
              style: ElevatedButton.styleFrom(
                primary: AppColor.primaryColor,
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
