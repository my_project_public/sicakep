import 'package:get/get.dart';

import '../controllers/history_laporan_controller.dart';

class HistoryLaporanBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HistoryLaporanController>(
      () => HistoryLaporanController(),
    );
  }
}
