import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/models/models.dart';
import 'package:ppa_lapor/services/services.dart';

class HistoryLaporanController extends GetxController {
  TextEditingController emailC = TextEditingController();
  RxBool isLoadingAddLaporan = RxBool(false);
  RxBool isLoadingLaporan = RxBool(false);
  ApiReturnValue laporan =
      ApiReturnValue(value: null, message: null, statusCode: null);
  @override
  void onInit() {
    getLaporan("0");
    super.onInit();
  }

  void getLaporan(String skip) async {
    try {
      isLoadingLaporan.value = true;
      ApiReturnValue category1 =
          await LaporanServices.getLaporan(skip) as ApiReturnValue;
      laporan = category1;
    } finally {
      isLoadingLaporan.value = false;
    }
  }

  void addLaporan(String skip) async {
    try {
      isLoadingAddLaporan.value = true;
      ApiReturnValue category1 =
          await LaporanServices.getLaporan(skip) as ApiReturnValue;
      if (category1.value.length > 0) {
        List<Laporan> items = category1.value;
        laporan.value.addAll(items);
      }
    } finally {
      isLoadingAddLaporan.value = false;
    }
  }
}
