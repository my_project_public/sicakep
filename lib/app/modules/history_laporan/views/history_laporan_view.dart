import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/alert/custom_alert.dart';
import 'package:ppa_lapor/app/widgets/custom_input.dart';

import '../controllers/history_laporan_controller.dart';

// ignore: must_be_immutable
class HistoryLaporanView extends GetView<HistoryLaporanController> {
  HistoryLaporanView({Key? key}) : super(key: key);
  ScrollController scrollController = ScrollController();

  void onScroll() {
    double maxScroll = scrollController.position.maxScrollExtent;
    double currentScroll = scrollController.position.pixels;

    print(currentScroll);
    print("currentScroll");

    print(maxScroll);
    print("maxScroll");

    if (currentScroll == maxScroll) {
      print("lol");
      print(currentScroll);
      print(maxScroll);
      controller.addLaporan(controller.laporan.value.length.toString());
    }

    currentScroll = maxScroll;
  }

  @override
  Widget build(BuildContext context) {
    scrollController.addListener(onScroll);

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        centerTitle: true,
        title: Text(
          'History Laporan',
          style: TextStyle(
            fontFamily: 'inter',
            fontSize: 18,
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Get.toNamed(Routes.HOME);
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border:
                  Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
          controller: scrollController,
          padding: EdgeInsets.symmetric(horizontal: 4, vertical: 16),
          child: Column(
            children: [
              Obx(() {
                if (controller.isLoadingLaporan.value) {
                  return Container(
                    margin: EdgeInsets.only(top: 20),
                    child: ListView.builder(
                      primary: false,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return Center(child: CircularProgressIndicator());
                      },
                      itemCount: controller.isLoadingAddLaporan.value
                          ? (controller.laporan.value != null)
                              ? controller.laporan.value.length + 10
                              : 10
                          : (controller.laporan.value != null)
                              ? controller.laporan.value.length + 10
                              : 10,
                    ),
                  );
                } else if (controller.laporan.value.length > 0) {
                  return ListView.builder(
                    itemCount: controller.laporan.value.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                    itemBuilder: (context, index) {
                      return LaporanTile(
                        onTap: () {
                          print("run");
                        },
                        userName: controller.laporan.value[index].userName,
                        jenisKekerasan:
                            controller.laporan.value[index].jenisKekerasan,
                        date: controller.laporan.value[index].created_at,
                        userImage: controller.laporan.value[index].userImage,
                        tipe: controller.laporan.value[index].tipe,
                        usia: controller.laporan.value[index].usia,
                        status: controller.laporan.value[index].status,
                      );
                    },
                  );
                } else {
                  // ignore: curly_braces_in_flow_control_structures, avoid_unnecessary_containers
                  return Container(child: Text("Terjadi Kesalahan!"));
                }
              }),
              const SizedBox(height: 40)
            ],
          )),
    );
  }
}

class LaporanTile extends StatelessWidget {
  final String userName;
  final String date;
  final String jenisKekerasan;
  final int status;

  final String userImage;
  final String usia;
  final String tipe;
  final void Function() onTap;

  LaporanTile({
    required this.onTap,
    required this.userName,
    required this.date,
    required this.userImage,
    required this.usia,
    required this.tipe,
    required this.status,
    required this.jenisKekerasan,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      borderRadius: BorderRadius.circular(18),
      child: Container(
        width: 381,
        margin: EdgeInsets.only(top: 10),
        height: 100,
        decoration: BoxDecoration(
            color: Colors.grey[200], borderRadius: BorderRadius.circular(16)),
        child: Row(
          children: [
            Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: CircleAvatar(
                  backgroundImage: NetworkImage(
                    (userImage != null)
                        ? userImage
                        : "https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg.jpg",
                  ),
                  radius: 50.0,
                )),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(userName,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold)),

                    SizedBox(
                      height: 3,
                    ),
                    // ignore: avoid_unnecessary_containers
                    Text(usia,
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 10, color: Colors.grey)),
                    SizedBox(
                      height: 3,
                    ),
                    // ignore: avoid_unnecessary_containers
                    Text(jenisKekerasan,
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 10, color: Colors.grey)),

                    // ignore: sized_box_for_whitespace
                    Row(
                      children: [
                        Container(
                          child: Align(
                            alignment: Alignment.bottomLeft,
                            child: Container(
                              margin: EdgeInsets.all(4),
                              padding: EdgeInsets.all(5),
                              height: 23,
                              width: 79,
                              decoration: BoxDecoration(
                                  color: Color(0xff21499c),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                tipe,
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(color: Colors.white, fontSize: 9),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              margin: EdgeInsets.all(4),
                              padding: EdgeInsets.all(5),
                              height: 23,
                              width: 79,
                              decoration: BoxDecoration(
                                  color: (status == 0)
                                      ? Color.fromARGB(255, 255, 120, 79)
                                      : (status == 1)
                                          ? Colors.blue[300]
                                          : Colors.red[600],
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                (status == 0)
                                    ? "Pending"
                                    : (status == 1)
                                        ? "Diterima"
                                        : "Ditolak",
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(color: Colors.white, fontSize: 9),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}
