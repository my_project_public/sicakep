import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/alert/custom_alert.dart';
import 'package:ppa_lapor/app/widgets/custom_input.dart';

import '../controllers/forget_password_controller.dart';

class ForgetPasswordView extends GetView<ForgetPasswordController> {
  const ForgetPasswordView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border:
                  Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        children: [
          // Welcome Title
          Container(
            margin: EdgeInsets.only(bottom: 32),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Lupa Password',
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 26,
                    fontFamily: 'inter',
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  'Masukkan Email Anda untuk mendapatkan reset kode passwod.',
                  style: TextStyle(
                    color: Color(0xFF727F8D),
                    fontFamily: 'inter',
                  ),
                ),
              ],
            ),
          ),
          // Form
          // Password Lama
          CustomInput(
            controller: controller.emailC,
            label: 'Email*',
            hint: 'Masukkan Email',
            cek: false,
            cekArray: false,
            prefixIcon: Container(
              padding: EdgeInsets.all(16),
              child: Icon(Icons.email),
            ),
            margin: EdgeInsets.only(bottom: 24),
          ),
          Obx(() {
            return SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 48,
              child: ElevatedButton(
                child: Text(
                  (controller.isLoading.value == true)
                      ? 'Loading...'
                      : 'Sumbit',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'inter'),
                ),
                onPressed: () {
                  if (controller.isLoading.value == true) {
                    return;
                  }
                  controller.isLoading.value = true;

                  if (controller.emailC.text.length < 1) {
                    CustomAlert.show(
                      title: 'Email',
                      cek: true,
                      description: "Email Tidak Boleh kosong!",
                      onTap: () {},
                    );
                    return;
                  }

                  controller.forgotPassword(controller.emailC.text);

                  Future.delayed(Duration(milliseconds: 2000), () {
                    print(controller.forgot.statusCode);
                    print(controller.forgot.statusCode);

                    print(controller.forgot.statusCode);
                    if (controller.forgot.statusCode == 200) {
                      CustomAlert.show(
                        title: 'Email Dikirim',
                        description:
                            "Silahkan Check Email Anda untuk mengganti password akun anda!",
                        onTap: () {
                          Get.toNamed(Routes.LOGIN);
                        },
                      );
                    } else {
                      CustomAlert.show(
                        title: 'Forgot Password Gagal',
                        cek: true,
                        description: controller.forgot.message,
                        onTap: () {},
                      );
                      return;
                    }
                  });
                },
                style: ElevatedButton.styleFrom(
                  primary: AppColor.primaryColor,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                ),
              ),
            );
          })
        ],
      ),
    );
  }
}
