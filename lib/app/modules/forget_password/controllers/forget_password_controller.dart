import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/models/models.dart';
import 'package:ppa_lapor/services/services.dart';

class ForgetPasswordController extends GetxController {
  TextEditingController emailC = TextEditingController();
  RxBool isLoading = RxBool(false);
  ApiReturnValue forgot =
      ApiReturnValue(value: null, message: null, statusCode: null);

  void forgotPassword(String email) async {
    try {
      isLoading.value = true;
      ApiReturnValue category1 =
          await AuthServices.forgotPassword(email) as ApiReturnValue;
      forgot = category1;
    } finally {
      isLoading.value = false;
    }
  }
}
