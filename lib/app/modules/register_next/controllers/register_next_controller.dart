import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/models/models.dart';
import 'package:ppa_lapor/services/services.dart';

class RegisterNextController extends GetxController {
  RxBool obsecureText = RxBool(true);
  TextEditingController nameC = TextEditingController();
  TextEditingController ttlC = TextEditingController();
  TextEditingController jenisConfirmC = TextEditingController();
  TextEditingController agamaConfirmC = TextEditingController();
  TextEditingController alamatConfirmC = TextEditingController();
  final List<String> items = [
    'Pria',
    'Wanita',
  ];
  RxString selectedValue = RxString('Pria');

  final List<String> itemsAgama = [
    'Islam',
    'Kristen Protestan',
    'Kristen Katolik',
    'Hindu',
    'Buddha',
    'Konghucu',
  ];
  final List<Nikah> itemsNikah = [
    Nikah("Pilih", "-1"),
    Nikah("Belum Menikah", "0"),
    Nikah("Sudah Menikah", "1"),
  ];
  RxString selectedAgamaValue = RxString('Islam');
  RxString selectedNikahValue = RxString("-1");

  RegisterRequest registerRequest = Get.arguments;
  ApiReturnValue user =
      ApiReturnValue(value: null, message: null, statusCode: null);

  RxBool isLoading = RxBool(false);
  @override
  void onClose() {
    nameC.text = '';
    ttlC.text = '';
    alamatConfirmC.text = '';
    super.onClose();
  }

  void register(RegisterRequest request) async {
    try {
      isLoading.value = true;
      ApiReturnValue user1 =
          await AuthServices.register(request) as ApiReturnValue;
      // ignore: unnecessary_null_comparison
      // if (user1.value != null || user1.message != null) {
      // ignore: unnecessary_cast
      user = user1;
      print(user);
      // }
    } finally {
      isLoading.value = false;
    }
  }
}
