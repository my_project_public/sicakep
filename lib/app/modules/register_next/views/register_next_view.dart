import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ppa_lapor/app/modules/register/controllers/register_controller.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';
import 'package:ppa_lapor/app/style/app_color.dart';
import 'package:ppa_lapor/app/widgets/alert/custom_alert.dart';
import 'package:ppa_lapor/app/widgets/custom_input.dart';

import '../controllers/register_next_controller.dart';

class RegisterNextView extends GetView<RegisterNextController> {
  const RegisterNextView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        title: Text(
          'Daftar',
          style: TextStyle(
            fontFamily: 'inter',
            fontSize: 26,
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border:
                  Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 24),
        children: [
          SizedBox(height: 32),
          Text(
            'Masukkan info untuk melanjutkan',
            style: TextStyle(
              color: AppColor.grey,
              fontFamily: 'inter',
              fontWeight: FontWeight.w700,
            ),
          ),
          CustomInput(
            maxLines: null,
            controller: controller.nameC,
            label: 'Nama*',
            hint: 'Masukkan Nama',
            cek: false,
            cekArray: false,
            prefixIcon: Icon(Icons.person, color: AppColor.grey),
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),
          CustomInput(
            maxLines: null,
            controller: controller.ttlC,
            label: 'Tanggal Lahir*',
            hint: '08XXXXXXXXXX',
            cek: true,
            cekArray: false,
            prefixIcon: Icon(Icons.phone, color: AppColor.grey),
            margin: EdgeInsets.only(bottom: 16),
          ),
          Text(
            'Agama*',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w500,
              fontFamily: 'inter',
            ),
          ),
          SizedBox(height: 4),
          Obx(
            () => SizedBox(
              height: 48,
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                  buttonDecoration: BoxDecoration(
                    border: Border.all(color: AppColor.grey, width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hint: Text(
                    'Select Item',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).hintColor),
                  ),
                  items: controller.itemsAgama
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(item),
                          ))
                      .toList(),
                  value: controller.selectedAgamaValue.value,
                  onChanged: (value) {
                    controller.selectedAgamaValue.value = value as String;
                  },
                  buttonHeight: 40,
                  buttonWidth: 140,
                  itemHeight: 40,
                ),
              ),
            ),
          ),

          Text(
            'Jenis Kelamin*',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w500,
              fontFamily: 'inter',
            ),
          ),
          SizedBox(height: 4),
          Obx(
            () => SizedBox(
              height: 48,
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                  buttonDecoration: BoxDecoration(
                    border: Border.all(color: AppColor.grey, width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hint: Text(
                    'Select Item',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).hintColor),
                  ),
                  items: controller.items
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(item),
                          ))
                      .toList(),
                  value: controller.selectedValue.value,
                  onChanged: (value) {
                    controller.selectedValue.value = value as String;
                  },
                  buttonHeight: 40,
                  buttonWidth: 140,
                  itemHeight: 40,
                ),
              ),
            ),
          ),
          SizedBox(height: 4),

          Text(
            'Sudah Menikah?*',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w500,
              fontFamily: 'inter',
            ),
          ),
          Obx(
            () => SizedBox(
              height: 48,
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                  buttonDecoration: BoxDecoration(
                    border: Border.all(color: AppColor.grey, width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hint: Text(
                    'Status Nikah',
                    style: TextStyle(
                        fontSize: 14, color: Theme.of(context).hintColor),
                  ),
                  items: controller.itemsNikah
                      .map((item) => DropdownMenuItem<String>(
                            value: item.id,
                            child: Text(item.name),
                          ))
                      .toList(),
                  value: controller.selectedNikahValue.value,
                  onChanged: (value) {
                    controller.selectedNikahValue.value = value as String;
                  },
                  buttonHeight: 40,
                  buttonWidth: 140,
                  itemHeight: 40,
                ),
              ),
            ),
          ),

          // Obx(
          //   () => SizedBox(
          //     height: 48,
          //     child: DropdownButtonHideUnderline(
          //       child: DropdownButton2(
          //         buttonPadding: EdgeInsets.symmetric(horizontal: 16),
          //         buttonDecoration: BoxDecoration(
          //           border: Border.all(color: AppColor.grey, width: 1),
          //           borderRadius: BorderRadius.circular(8),
          //         ),
          //         hint: Text(
          //           'Status Nikah',
          //           style: TextStyle(
          //               fontSize: 14, color: Theme.of(context).hintColor),
          //         ),
          //         items: controller.itemsNikah
          //             .map((item) => DropdownMenuItem<String>(
          //                   value: item.id,
          //                   child: Text(item.name),
          //                 ))
          //             .toList(),
          //         value: controller.selectedNikahValue.value,
          //         onChanged: (value) {
          //           controller.selectedNikahValue.value = value as String;
          //         },
          //         buttonHeight: 40,
          //         buttonWidth: 140,
          //         itemHeight: 40,
          //       ),
          //     ),
          //   ),
          // ),
          CustomInput(
            maxLines: null,
            controller: controller.alamatConfirmC,
            label: 'Alamat*',
            hint: 'Masukkan Alamat',
            cek: false,
            cekArray: false,
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),

          Obx(
            () => Container(
              width: MediaQuery.of(context).size.width,
              height: 48,
              margin: EdgeInsets.only(top: 8),
              child: ElevatedButton(
                child: (controller.isLoading.value == true)
                    ? Text(
                        'Loading...',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'inter'),
                      )
                    : Text(
                        'Daftar',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'inter'),
                      ),
                onPressed: () {
                  if ((controller.isLoading.value == true)) {
                    return;
                  }
                  if (controller.nameC.text.length < 1) {
                    CustomAlert.show(
                      title: 'Nama',
                      cek: true,
                      description: "Nama Tidak Boleh kosong!",
                      onTap: () {},
                    );
                    return;
                  }
                  if (controller.ttlC.text.length < 1) {
                    CustomAlert.show(
                      title: 'Tanggal Lahir',
                      cek: true,
                      description: "Tanggal Lahir tidak boleh kosong!",
                      onTap: () {},
                    );
                    return;
                  }
                  if (controller.selectedAgamaValue.value.length < 1) {
                    CustomAlert.show(
                      title: 'Jenis Kelamin',
                      cek: true,
                      description: "Jenis Kelamin tidak boleh kosong!",
                      onTap: () {},
                    );
                    return;
                  }
                  if (controller.selectedValue.value.length < 1) {
                    CustomAlert.show(
                      title: 'Agama',
                      cek: true,
                      description: "Agama tidak boleh kosong!",
                      onTap: () {},
                    );
                    return;
                  }
                  if (controller.alamatConfirmC.text.length < 1) {
                    CustomAlert.show(
                      title: 'Alamat',
                      cek: true,
                      description: "Alamat tidak boleh kosong!",
                      onTap: () {},
                    );
                    return;
                  }
                  controller.registerRequest.name = controller.nameC.text;
                  controller.registerRequest.ttl = controller.ttlC.text;
                  controller.registerRequest.name = controller.nameC.text;
                  controller.registerRequest.gender =
                      controller.selectedValue.value;
                  controller.registerRequest.alamat =
                      controller.alamatConfirmC.text;
                  controller.registerRequest.agama =
                      controller.selectedAgamaValue.value;
                  controller.registerRequest.isMerried =
                      controller.selectedNikahValue.value;

                  controller.register(controller.registerRequest);
                  Future.delayed(Duration(milliseconds: 5000), () {
                    print("code");
                    print(controller.user.statusCode);
                    if (controller.isLoading.value == false) {
                      print(controller.user.statusCode);
                      if (controller.user.statusCode == 200) {
                        CustomAlert.show(
                          title: 'Register Success',
                          description:
                              "Silahkan Check Email Anda untuk memverifikasi akun anda!",
                          onTap: () {
                            Get.delete<RegisterController>();
                            Get.delete<RegisterNextController>();
                            Get.toNamed(Routes.LOGIN);
                          },
                        );
                      } else {
                        CustomAlert.show(
                          title: 'Register Gagal',
                          description:
                              "Email atau Nomor handphone sudah digunakan",
                          onTap: () {},
                        );
                      }
                    } else {}
                  });
                },
                style: ElevatedButton.styleFrom(
                  primary: AppColor.primaryColor,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                ),
              ),
            ),
          ),

          // Login
          Container(
            margin: EdgeInsets.only(top: 32),
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Sudah memiliki akun?',
                  style: TextStyle(
                    fontFamily: 'inter',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Get.toNamed(Routes.LOGIN);
                  },
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontFamily: 'inter',
                      fontWeight: FontWeight.w600,
                      color: AppColor.primaryColor,
                    ),
                  ),
                  style: TextButton.styleFrom(
                    primary: AppColor.primaryColor,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
