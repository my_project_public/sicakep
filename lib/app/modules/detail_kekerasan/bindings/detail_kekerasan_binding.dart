import 'package:get/get.dart';

import '../controllers/detail_kekerasan_controller.dart';

class DetailKekerasanBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailKekerasanController>(
      () => DetailKekerasanController(),
    );
  }
}
