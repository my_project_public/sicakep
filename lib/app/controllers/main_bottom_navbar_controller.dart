import 'package:get/get.dart';
import 'package:ppa_lapor/app/routes/app_pages.dart';

class MainBottomNavbarController extends GetxController {
  RxInt pageIndex = 0.obs;

  void changePage(int index) {
    pageIndex.value = index;
    switch (index) {
      case 1:
        Get.offAllNamed(Routes.LAPOR1);
        break;
      case 2:
        Get.offAllNamed(Routes.LIVE_CHAT);
        break;
      case 3:
        Get.offAllNamed(Routes.PENGATURAN);
        break;
      default:
        Get.offAllNamed(Routes.HOME);
        break;
    }
  }
}
