import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/app/controllers/main_bottom_navbar_controller.dart';
import 'package:ppa_lapor/app/style/app_color.dart';

class MainBottomNavbar extends GetView<MainBottomNavbarController> {
  const MainBottomNavbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 72,
      child: BottomNavigationBar(
        backgroundColor: Colors.white,
        currentIndex: controller.pageIndex.value,
        onTap: controller.changePage,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        selectedItemColor: AppColor.primaryColor,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'assets/icons/home.svg',
              color: (controller.pageIndex.value == 0)
                  ? AppColor.primaryColor
                  : AppColor.grey,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'assets/icons/invoice.svg',
              color: (controller.pageIndex.value == 1)
                  ? AppColor.primaryColor
                  : AppColor.grey,
            ),
            label: 'Lapor',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'assets/icons/comment.svg',
              color: (controller.pageIndex.value == 2)
                  ? AppColor.primaryColor
                  : AppColor.grey,
            ),
            label: 'Konsultasi',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'assets/icons/settings.svg',
              color: (controller.pageIndex.value == 3)
                  ? AppColor.primaryColor
                  : AppColor.grey,
            ),
            label: 'Pengaturan',
          ),
        ],
      ),
    );
  }
}
