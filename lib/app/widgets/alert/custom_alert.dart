import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:ppa_lapor/app/style/app_color.dart';

class CustomAlert {
  static show({
    required String title,
    String? description,
     bool cek = false,
    required void Function()? onTap,
  }) {
    Get.defaultDialog(
      title: "",
      radius: 8,
      titlePadding: EdgeInsets.zero,
      titleStyle: TextStyle(fontSize: 0),
      content: Container(
        width: Get.width,
        child: Column(
          children: [
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                icon: Icon(Icons.close_sharp),
                onPressed: () {
                  Get.back();
                },
              ),
            ),
            Container(
              width: 130,
              height: 130,
              padding: EdgeInsets.all(36),
              decoration: BoxDecoration(
                color: AppColor.primaryColor,
                borderRadius: BorderRadius.circular(100),
              ),
              child:(cek) ? Container(
                child: Icon(
                  Icons.close,
                  size: 60.0,
                  color: Colors.white,
                ),
              ) : SvgPicture.asset(
                'assets/icons/check.svg',
                color: Colors.white,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 32, bottom: 8),
              child: Text(
                '$title',
                style: TextStyle(
                  fontSize: 26,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'inter',
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 32),
              margin: EdgeInsets.only(bottom: 64),
              child: Text(
                (description != null) ? description : '',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: AppColor.grey,
                  fontFamily: 'inter',
                ),
              ),
            ),
            Container(
              width: Get.width,
              height: 48,
              margin: EdgeInsets.only(left: 16, right: 16, bottom: 8),
              child: ElevatedButton(
                child: Text(
                  'Selesai',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, fontFamily: 'inter'),
                ),
                onPressed: onTap,
                style: ElevatedButton.styleFrom(
                  primary: AppColor.primaryColor,
                  elevation: 0,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
