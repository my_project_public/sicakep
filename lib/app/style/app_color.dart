import 'package:flutter/cupertino.dart';

class AppColor {
  static const Color primaryColor = Color(0xFF8F02FD);
  static const Color grey = Color(0xFF8E8EA1);
}
