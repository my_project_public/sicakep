import 'package:get/get.dart';
import 'package:ppa_lapor/app/modules/detail_berita/bindings/detail_berita_binding.dart';
import 'package:ppa_lapor/app/modules/detail_berita/views/detail_berita_view.dart';
import 'package:ppa_lapor/app/modules/history_laporan/bindings/history_laporan_binding.dart';
import 'package:ppa_lapor/app/modules/history_laporan/views/history_laporan_view.dart';
import 'package:ppa_lapor/app/modules/register_next/bindings/registe_next_binding.dart';
import 'package:ppa_lapor/app/modules/register_next/views/register_next_view.dart';

import '../modules/change_password/bindings/change_password_binding.dart';
import '../modules/change_password/views/change_password_view.dart';
import '../modules/detail_kekerasan/bindings/detail_kekerasan_binding.dart';
import '../modules/detail_kekerasan/views/detail_kekerasan_view.dart';
import '../modules/forget_password/bindings/forget_password_binding.dart';
import '../modules/forget_password/views/forget_password_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/korban/bindings/korban_binding.dart';
import '../modules/korban/views/korban_view.dart';
import '../modules/lapor/bindings/lapor_binding.dart';
import '../modules/lapor/views/lapor_view.dart';
import '../modules/lapor_kekerasan/bindings/lapor_kekerasan_binding.dart';
import '../modules/lapor_kekerasan/views/lapor_kekerasan_view.dart';
import '../modules/live_chat/bindings/live_chat_binding.dart';
import '../modules/live_chat/views/live_chat_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/pengaturan/bindings/pengaturan_binding.dart';
import '../modules/pengaturan/views/pengaturan_view.dart';
import '../modules/register/bindings/register_binding.dart';
import '../modules/register/views/register_view.dart';
import '../modules/reset_password/bindings/reset_password_binding.dart';
import '../modules/reset_password/views/reset_password_view.dart';
import '../modules/saksi_mata/bindings/saksi_mata_binding.dart';
import '../modules/saksi_mata/views/saksi_mata_view.dart';
import '../modules/tentang/bindings/tentang_binding.dart';
import '../modules/tentang/views/tentang_view.dart';
import '../modules/welcome/bindings/welcome_binding.dart';
import '../modules/welcome/views/welcome_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.WELCOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
      transition: Transition.noTransition,
      transitionDuration: Duration.zero,
    ),
    GetPage(
      name: _Paths.LAPOR1,
      page: () => const LaporView(),
      binding: LaporBinding(),
      transition: Transition.noTransition,
      transitionDuration: Duration.zero,
    ),
    GetPage(
      name: _Paths.PENGATURAN,
      page: () => const PengaturanView(),
      binding: PengaturanBinding(),
      transition: Transition.noTransition,
      transitionDuration: Duration.zero,
    ),
    GetPage(
      name: _Paths.TENTANG,
      page: () => const TentangView(),
      binding: TentangBinding(),
    ),
    GetPage(
      name: _Paths.CHANGE_PASSWORD,
      page: () => const ChangePasswordView(),
      binding: ChangePasswordBinding(),
    ),
    GetPage(
      name: _Paths.WELCOME,
      page: () => WelcomeView(),
      binding: WelcomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.FORGET_PASSWORD,
      page: () => const ForgetPasswordView(),
      binding: ForgetPasswordBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => const RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: _Paths.REGISTERNEXT,
      page: () => const RegisterNextView(),
      binding: RegisterNextBinding(),
    ),
    GetPage(
      name: _Paths.RESET_PASSWORD,
      page: () => const ResetPasswordView(),
      binding: ResetPasswordBinding(),
    ),
    GetPage(
      name: _Paths.SAKSI_MATA,
      page: () => const SaksiMataView(),
      binding: SaksiMataBinding(),
    ),
    GetPage(
      name: _Paths.LAPOR_KEKERASAN,
      page: () => LaporKekerasanView(),
      binding: LaporKekerasanBinding(),
    ),
    GetPage(
      name: _Paths.KORBAN,
      page: () => const KorbanView(),
      binding: KorbanBinding(),
    ),
    GetPage(
      name: _Paths.LIVE_CHAT,
      page: () => const LiveChatView(),
      binding: LiveChatBinding(),
    ),
    GetPage(
      name: _Paths.HISTORY_LAPORAN,
      page: () => HistoryLaporanView(),
      binding: HistoryLaporanBinding(),
      transition: Transition.noTransition,
      transitionDuration: Duration.zero,
    ),
    GetPage(
      transition: Transition.noTransition,
      transitionDuration: Duration.zero,
      name: _Paths.DETAIL_KEKERASAN,
      page: () => const DetailKekerasanView(),
      binding: DetailKekerasanBinding(),
    ),
    GetPage(
      transition: Transition.noTransition,
      transitionDuration: Duration.zero,
      name: _Paths.DETAIL_BERITA,
      page: () => const DetailBeritaView(),
      binding: DetailBeritaBinding(),
    ),
  ];
}
