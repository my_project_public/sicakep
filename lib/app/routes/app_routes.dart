part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const LAPOR1 = _Paths.LAPOR1;
  static const PENGATURAN = _Paths.PENGATURAN;
  static const TENTANG = _Paths.TENTANG;
  static const CHANGE_PASSWORD = _Paths.CHANGE_PASSWORD;
  static const WELCOME = _Paths.WELCOME;
  static const LOGIN = _Paths.LOGIN;
  static const FORGET_PASSWORD = _Paths.FORGET_PASSWORD;
  static const REGISTER = _Paths.REGISTER;
  static const REGISTERNEXT = _Paths.REGISTERNEXT;

  static const RESET_PASSWORD = _Paths.RESET_PASSWORD;
  static const SAKSI_MATA = _Paths.SAKSI_MATA;
  static const LAPOR_KEKERASAN = _Paths.LAPOR_KEKERASAN;
  static const KORBAN = _Paths.KORBAN;
  static const LIVE_CHAT = _Paths.LIVE_CHAT;
  static const HISTORY_LAPORAN = _Paths.HISTORY_LAPORAN;
  static const DETAIL_KEKERASAN = _Paths.DETAIL_KEKERASAN;
  static const DETAIL_BERITA = _Paths.DETAIL_BERITA;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const LAPOR1 = '/lapor';
  static const PENGATURAN = '/pengaturan';
  static const TENTANG = '/tentang';
  static const CHANGE_PASSWORD = '/change-password';
  static const WELCOME = '/welcome';
  static const LOGIN = '/login';
  static const FORGET_PASSWORD = '/forget-password';
  static const REGISTER = '/register';
  static const REGISTERNEXT = '/register-next';
  static const RESET_PASSWORD = '/reset-password';
  static const SAKSI_MATA = '/saksi-mata';
  static const LAPOR_KEKERASAN = '/lapor-kekerasan';
  static const KORBAN = '/korban';
  static const LIVE_CHAT = '/live-chat';
  static const HISTORY_LAPORAN = '/laporan';
  static const DETAIL_KEKERASAN = '/detail-kekerasan';
  static const DETAIL_BERITA = '/detail-berita';
}
