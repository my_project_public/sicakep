part of 'models.dart';

class Chat extends Equatable {
  int id;
  int userId;
  String? name;
  String? image;
  String? message;
  String? created_at;

  Chat(
      {required this.id,
      required this.userId,
      this.name,
      this.message,
      this.created_at,
      this.image});

  factory Chat.fromJson(Map<String, dynamic> data) => Chat(
      id: data['id'],
      userId: data['user_id'],
      message: data['message'],
      name: data['name_user'],
      image: data['image'],
      created_at: data['created_at']);
  Chat copyWith({
    String? name,
    String? message,
    String? image,
    String? created_at,
    required int id,
  }) {
    return Chat(
        name: name ?? this.name,
        userId: userId,
        id: id,
        message: message ?? this.message,
        image: image ?? this.image,
        created_at: created_at ?? this.created_at);
  }

  @override
  List<Object> get props =>
      [id, userId, name ?? '', message ?? '', created_at ?? ""];
}
