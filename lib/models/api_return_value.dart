part of 'models.dart';

class ApiReturnValue<T> {
  late T? value;
  late final String? message;
  late final int? statusCode;

  ApiReturnValue({this.value, this.message, this.statusCode});
}
