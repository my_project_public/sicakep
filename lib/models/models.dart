import 'dart:io';

import 'package:equatable/equatable.dart';

part 'api_return_value.dart';
part 'user.dart';
part 'category.dart';
part 'berita.dart';
part 'Laporan.dart';
part 'request/register_request.dart';
part 'request/login_request.dart';
part 'request/korban_request.dart';
part 'request/update_profile_request.dart';
part 'chat.dart';

part 'nikah.dart';
