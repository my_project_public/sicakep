part of 'models.dart';

class Nikah {
  String name;
  String id;
  Nikah(this.name, this.id);
  @override
  String toString() {
    return '{ ${this.name}, ${this.id} }';
  }
}
