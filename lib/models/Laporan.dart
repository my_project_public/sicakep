part of 'models.dart';

class Laporan extends Equatable {
  int id;
  int? status;
  String? jenisKekerasan;
  String? userName;
  String? userImage;
  String? usia;
  String? gender;
  String? tipeId;
  String? tipe;
  String? created_at;

  Laporan({
    required this.id,
    this.status,
    this.jenisKekerasan,
    this.userName,
    this.userImage,
    this.usia,
    this.gender,
    this.tipeId,
    this.tipe,
    this.created_at,
  });

  factory Laporan.fromJson(Map<String, dynamic> data) => Laporan(
      id: data['id'],
      status: data['status'],
      jenisKekerasan: data['jenis_kekerasan'],
      userName: data['user_name'],
      userImage: data['user_image'],
      usia: data['usia'],
      gender: data['jns_kelamin'],
      tipeId: data['tipe_id'].toString(),
      tipe: data['tipe'],
      created_at: data['created_at']);
  Laporan copyWith({
    String? jenisKekerasan,
    String? userName,
    String? userImage,
    String? usia,
    String? gender,
    String? tipeId,
    String? tipe,
    String? created_at,
    int? status,
    required int id,
  }) {
    return Laporan(
        jenisKekerasan: jenisKekerasan ?? this.jenisKekerasan,
        userName: userName ?? this.userName,
        id: id,
        userImage: userImage ?? this.userImage,
        usia: usia ?? this.usia,
        gender: gender ?? this.gender,
        tipeId: tipeId ?? this.tipeId,
        tipe: tipe ?? this.tipe,
        status: status ?? this.status,
        created_at: created_at ?? this.created_at);
  }

  @override
  List<Object> get props => [
        id,
        jenisKekerasan ?? '',
        userName ?? '',
        usia ?? '',
        gender ?? '',
        tipeId ?? '',
        tipe ?? '',
        created_at ?? "",
        status ?? 0
      ];
}
