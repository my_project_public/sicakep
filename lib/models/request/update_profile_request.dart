part of '../models.dart';

class UpdateProfileRequest {
  String? name;
  String? ttl;
  String? email;

  String? gender;
  String? alamat;
  String? agama;
  String? phone;
  String? isMerried;

  String? passwordLamaC;
  String? passwordBaruC;
  String? passwordBaruKonfirmasiC;
  File? image;

  UpdateProfileRequest(
      {this.name,
      this.email,
      this.ttl,
      this.gender,
      this.alamat,
      this.agama,
      this.phone,
      this.image,
      this.isMerried,
      this.passwordLamaC,
      this.passwordBaruC,
      this.passwordBaruKonfirmasiC});
}
