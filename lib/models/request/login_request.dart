part of '../models.dart';


// ignore: must_be_immutable
class LoginRequest{
  String? email; 
  String? password;

  LoginRequest({
    this.email,
    this.password
  });

}
