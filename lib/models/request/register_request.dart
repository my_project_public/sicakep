part of '../models.dart';

// ignore: must_be_immutable
class RegisterRequest {
  String? email;
  String? phone;
  String? name;
  String? alamat;
  String? ttl;
  String? gender = "Laki-Laki";
  String? agama = "Islam";
  String? password;
  String? isMerried = "-1";
  String? hubungan;

  RegisterRequest(
      {this.email,
      this.phone,
      this.name,
      this.alamat,
      this.ttl,
      this.gender,
      this.agama,
      this.isMerried,
      this.password,
      this.hubungan});
}
