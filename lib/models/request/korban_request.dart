part of '../models.dart';

// ignore: must_be_immutable
class KorbanRequest {
  String? namaS;
  String? namaK;
  String? ttlS;
  String? ttlK;

  String? genderS;
  String? genderK;

  String? alamatS;
  String? alamatKo;

  String? hub;
  String? agamaS;
  String? agamaK;

  String? phoneS;
  String? phoneK;

  String? nikahS;
  String? nikahK;

  String? jenisKekerasanId;
  String? kronologi;
  String? alamatK;
  String? tglKejadian;
  int? jenis = 0;
  File? image;

  KorbanRequest(
      {this.jenisKekerasanId,
      this.kronologi,
      this.namaS,
      this.namaK,
      this.ttlS,
      this.ttlK,
      this.genderS,
      this.genderK,
      this.alamatS,
      this.alamatK,
      this.hub,
      this.agamaS,
      this.agamaK,
      this.phoneS,
      this.phoneK,
      this.jenis,
      this.alamatKo,
      this.image});
}
