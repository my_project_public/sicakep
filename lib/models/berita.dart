part of 'models.dart';

class News extends Equatable {
  int id;
  String? judul;
  String? url;
  String? image;
  String? created_at;
  String? desc;

  News(
      {required this.id,
      this.judul,
      this.url,
      this.created_at,
      this.image,
      this.desc});

  factory News.fromJson(Map<String, dynamic> data) => News(
        id: data['id'],
        judul: data['judul'],
        url: data['url'],
        image: data['image'],
        created_at: data['created_at'],
        desc: data['desc'],
      );
  News copyWith({
    String? judul,
    String? url,
    String? image,
    String? created_at,
    String? desc,
    required int id,
  }) {
    return News(
        judul: judul ?? this.judul,
        id: id,
        url: url ?? this.url,
        image: image ?? this.image,
        desc: desc ?? this.desc,
        created_at: created_at ?? this.created_at);
  }

  @override
  List<Object> get props =>
      [id, judul ?? '', url ?? '', created_at ?? "", desc ?? ""];
}
