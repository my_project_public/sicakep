part of 'models.dart';

class User extends Equatable {
  int id;
  String? name;
  String? email;
  String? alamat;
  String? phone;
  String? token;
  String? url;
  String? gender;
  String? agama;
  String? nikah;
  String? ttl;

  User(
      {required this.id,
      this.name,
      this.email,
      this.phone,
      this.url,
      this.gender,
      this.ttl,
      this.agama,
      this.nikah,
      this.alamat,
      this.token});

  factory User.fromJson(Map<String, dynamic> data) => User(
        id: data['id'],
        name: data['name'],
        email: data['email'],
        phone: data['phone'],
        url: data['avatar'],
        gender: data['jns_kelamin'],
        alamat: data['alamat'],
        agama: data['agama'],
        ttl: data['ttl'],
        nikah: data['nikah'],
        token: data['token'],
      );
  User copyWith({
    String? name,
    String? email,
    String? alamat,
    String? phone,
    String? token,
    String? url,
    String? gender,
    String? agama,
    String? nikah,
    String? ttl,
    required int id,
  }) {
    return User(
      name: name ?? this.name,
      email: email ?? this.email,
      phone: phone ?? this.phone,
      gender: gender ?? this.gender,
      id: id,
      agama: agama ?? this.agama,
      alamat: alamat ?? this.alamat,
      token: token ?? this.token,
      url: url ?? this.url,
      ttl: ttl ?? this.ttl,
      nikah: nikah ?? this.nikah,
    );
  }

  @override
  List<Object> get props => [
        id,
        name ?? '',
        email ?? '',
        phone ?? '',
        url ?? '',
        gender ?? '',
        alamat ?? ''
      ];
}
