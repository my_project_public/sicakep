part of 'models.dart';

class Category extends Equatable {
  int id;
  String? judul;
  String? url;
  String? desc;

  Category({
    required this.id,
    this.judul,
    this.url,
    this.desc,
  });

  factory Category.fromJson(Map<String, dynamic> data) => Category(
      id: data['id'],
      judul: data['judul'],
      url: data['url'],
      desc: data['desc']);
  Category copyWith({
    String? judul,
    String? image,
    required int id,
  }) {
    return Category(
      judul: judul ?? this.judul,
      id: id,
      url: url ?? this.url,
      desc: desc ?? this.desc,
    );
  }

  @override
  List<Object> get props => [id, judul ?? '', url ?? ''];
}
