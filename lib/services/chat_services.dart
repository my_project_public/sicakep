part of 'services.dart';

class ChatServices {
  static var client = http.Client();

  static Future<ApiReturnValue?> getChat(String skip) async {
    String? token = await AuthServices.getToken();

    var response = await client.post(Uri.parse(baseURL + 'chat'),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + token!
        },
        body: jsonEncode(<String, String>{
          'skip': skip.toString(),
          'take': 10.toString(),
        }));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      List<Chat> value =
          (data['result'] as Iterable).map((e) => Chat.fromJson(e)).toList();
      print(value);

      return ApiReturnValue(value: value, statusCode: response.statusCode);
    } else {
      var data = jsonDecode(response.body);
      // print(response.statusCode);
      return ApiReturnValue(
          message: data['message'], statusCode: response.statusCode);
    }
  }

  static Future<ApiReturnValue?> storeChat(String message) async {
    String? token = await AuthServices.getToken();

    var response = await client.post(Uri.parse(baseURL + 'store-chat'),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + token!
        },
        body: jsonEncode(<String, String>{
          'message': message.toString(),
        }));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      Chat value = Chat.fromJson(data['result']);
      print(value);

      return ApiReturnValue(value: value, statusCode: response.statusCode);
    } else {
      var data = jsonDecode(response.body);
      // print(response.statusCode);
      return ApiReturnValue(
          message: data['message'], statusCode: response.statusCode);
    }
  }
}
