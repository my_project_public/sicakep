part of 'services.dart';

class NewsServices {
  static var client = http.Client();

  static Future<ApiReturnValue?> getNews(String skip) async {
    var response = await client.post(Uri.parse(baseURL + 'news/get'),
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(<String, String>{
          'skip': skip.toString(),
          'take': 4.toString(),
        }));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);

      List<News> value =
          (data['result'] as Iterable).map((e) => News.fromJson(e)).toList();

      return ApiReturnValue(value: value, statusCode: response.statusCode);
    } else {
      var data = jsonDecode(response.body);
      // print(response.statusCode);
      return ApiReturnValue(
          message: data['message'], statusCode: response.statusCode);
    }
  }
}
