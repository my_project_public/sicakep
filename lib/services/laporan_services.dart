part of 'services.dart';

class LaporanServices {
  static var client = http.Client();

  static Future<ApiReturnValue?> storeKorban(KorbanRequest request) async {
    String? token = await AuthServices.getToken();
    http.MultipartRequest request1;
    print(request);
    var uri2 = Uri.parse(baseURL + 'store-korban');
    request1 = http.MultipartRequest("POST", uri2)
      ..headers["Content-Type"] = "application/json"
      ..headers["Authorization"] = "Bearer " + token!;
    if (request.image != null) {
      var multipartFile =
          await http.MultipartFile.fromPath('image', request.image!.path);
      request1.files.add(multipartFile);
    }

    request1.fields['jenis_kekerasan_id'] = request.jenisKekerasanId.toString();
    request1.fields['kronologi'] = request.kronologi.toString();
    request1.fields['tanggal_kejadian'] = request.tglKejadian.toString();
    request1.fields['alamat_kejadian'] = request.alamatK.toString();

    var response = await request1.send();
    String responseBody = await response.stream.bytesToString();
    print(responseBody);
    var data = jsonDecode(responseBody);
    print(data);

    print(response.statusCode);
    if (response.statusCode == 200) {
      print(response);

      return ApiReturnValue(
          message: "Berhasil menyimpan data!", statusCode: response.statusCode);
    } else {
      return ApiReturnValue(
          message: "Gagal menyimpan data!", statusCode: response.statusCode);
    }
  }

  static Future<ApiReturnValue?> storeSaksi(KorbanRequest request) async {
    print(request);
    String? token = await AuthServices.getToken();
    http.MultipartRequest request1;
    print(request);
    var uri2 = Uri.parse(baseURL + 'store-saksi');
    request1 = http.MultipartRequest("POST", uri2)
      ..headers["Content-Type"] = "application/json"
      ..headers["Authorization"] = "Bearer " + token!;
    if (request.image != null) {
      var multipartFile =
          await http.MultipartFile.fromPath('image', request.image!.path);
      request1.files.add(multipartFile);
    }

    request1.fields['jenis_kekerasan_id'] = request.jenisKekerasanId.toString();
    request1.fields['kronologi'] = request.kronologi.toString();
    request1.fields['tanggal_kejadian'] = request.tglKejadian.toString();
    request1.fields['alamat_kejadian'] = request.alamatK.toString();
    request1.fields['nama'] = request.namaK.toString();
    request1.fields['ttl'] = request.ttlK.toString();
    request1.fields['jns_kelamin'] = request.genderK.toString();
    request1.fields['alamat'] = request.alamatKo.toString();
    request1.fields['hub'] = request.hub.toString();
    request1.fields['agama'] = request.agamaK.toString();
    request1.fields['phone'] = request.phoneK.toString();
    request1.fields['is_married'] = request.nikahK.toString();

    var response = await request1.send();
    String responseBody = await response.stream.bytesToString();
    print(responseBody);
    var data = jsonDecode(responseBody);
    print(data);

    print(response.statusCode);
    if (response.statusCode == 200) {
      print(response);

      return ApiReturnValue(
          message: "Berhasil menyimpan data!", statusCode: response.statusCode);
    } else {
      return ApiReturnValue(
          message: "Gagal menyimpan data!", statusCode: response.statusCode);
    }
  }

  static Future<ApiReturnValue?> getLaporan(String skip) async {
    String? token = await AuthServices.getToken();

    var response = await client.post(Uri.parse(baseURL + 'laporan'),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + token!
        },
        body: jsonEncode(<String, String>{
          'skip': skip.toString(),
          'take': 8.toString(),
        }));
    print(response);

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print(data);
      List<Laporan> value =
          (data['result'] as Iterable).map((e) => Laporan.fromJson(e)).toList();
      return ApiReturnValue(value: value, statusCode: response.statusCode);
    } else {
      var data = jsonDecode(response.body);
      // print(response.statusCode);
      return ApiReturnValue(
          message: data['message'], statusCode: response.statusCode);
    }
  }
}
