part of 'services.dart';

class UserServices {
  static var client = http.Client();
  static Future<String?> getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('name');
  }

  static Future<String?> getAlamat() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('alamat');
  }

  static Future<String?> getJnsKelamin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('jns_kelamin');
  }

  static Future<String?> getTtl() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('ttl');
  }

  static Future<String?> getAgama() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('agama');
  }

  static Future<String?> getPicture() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('picturePath');
  }

  static Future<String?> getGender() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('gender');
  }

  static Future<String?> getPhone() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('phone');
  }

  static Future<String?> getEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('email');
  }

  static Future<bool?> getLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool('isLogin');
  }

  static void persistToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', token);
  }

  static void persistName(String name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('name', name);
  }

  static void persistLogin(bool name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('isLogin', name);
  }

  static void persistPicture(
    String picturePath,
  ) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('picturePath', picturePath);
  }

  static void persistEmail(String email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('email', email);
  }

  static void persistPhone(String phone) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone', phone);
  }

  static void persistAgama(String agama) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('agama', agama);
  }

  static void persistTtl(String ttl) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('ttl', ttl);
  }

  static void persistKelamin(String jns_kelamin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('jns_kelamin', jns_kelamin);
  }

  static void persistAlamat(String alamat) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('alamat', alamat);
  }

  static void persistNikah(String name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('nikah', name);
  }

  static Future<ApiReturnValue?> updateProfile(
      UpdateProfileRequest request) async {
    String? token = await AuthServices.getToken();
    http.MultipartRequest request1;
    print(request);
    var uri2 = Uri.parse(baseURL + 'user-update');
    request1 = http.MultipartRequest("POST", uri2)
      ..headers["Content-Type"] = "application/json"
      ..headers["Authorization"] = "Bearer " + token!;
    if (request.image != null) {
      var multipartFile =
          await http.MultipartFile.fromPath('avatar', request.image!.path);
      request1.files.add(multipartFile);
    }

    request1.fields['name'] = request.name.toString();
    request1.fields['email'] = request.email.toString();
    request1.fields['phone'] = request.phone.toString();
    request1.fields['alamat'] = request.alamat.toString();
    request1.fields['agama'] = request.agama.toString();
    request1.fields['jns_kelamin'] = request.gender.toString();
    request1.fields['ttl'] = request.ttl.toString();
    request1.fields['is_married'] = request.isMerried.toString();

    request1.fields['password'] = request.passwordBaruC.toString();
    request1.fields['password_old'] = request.passwordLamaC.toString();
    request1.fields['password_confirmation'] =
        request.passwordBaruKonfirmasiC.toString();

    print(request1);
    var response = await request1.send();
    String responseBody = await response.stream.bytesToString();
    print(responseBody);
    var data = jsonDecode(responseBody);
    print(data);

    print(response.statusCode);
    if (response.statusCode == 200) {
      print(response);
      User value = User.fromJson(data['data']);
      persistLogin(true);
      persistName(value.name!);
      persistEmail(value.email!);
      persistPhone(value.phone!);
      persistNikah(value.nikah!);

      persistAgama(value.agama ?? "Tidak ada");
      persistTtl(value.ttl ?? "Tidak ada");
      persistKelamin(value.gender ?? "Tidak ada");
      persistAlamat(value.alamat ?? "Tidak ada");
      persistPicture(value.url ?? "tidak ada");
      print(value.url);
      return ApiReturnValue(
          message: "Berhasil menyimpan data!", statusCode: response.statusCode);
    } else {
      return ApiReturnValue(
          message: "Gagal menyimpan data!", statusCode: response.statusCode);
    }
  }
}
