import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'package:ppa_lapor/models/models.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_services.dart';
part 'category_services.dart';
part 'news_services.dart';
part 'laporan_services.dart';
part 'user_services.dart';
part 'chat_services.dart';

String baseURL = 'https://mandor.absensinow.id/api/';
