part of 'services.dart';

class CategoryServices {
  static var client = http.Client();

 

  static Future<ApiReturnValue?> getCategory() async {
    var response = await client.get(Uri.parse(baseURL + 'category/get'),
        headers: {"Content-Type": "application/json"});
    
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);

      List<Category> value =
          (data['result'] as Iterable).map((e) => Category.fromJson(e)).toList();
      print(value);


      return ApiReturnValue(value: value, statusCode: response.statusCode);
    } else {
      var data = jsonDecode(response.body);
      // print(response.statusCode);
      return ApiReturnValue(message: data['message'], statusCode: response.statusCode);
    }
  }

  
}
