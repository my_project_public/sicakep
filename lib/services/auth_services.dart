part of 'services.dart';

class AuthServices {
  static var client = http.Client();

  static Future<bool> hasToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token') != null;
  }

  static Future<String?> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  static Future<String?> getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('name');
  }

  static Future<String?> getAlamat() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('alamat');
  }

  static Future<String?> getJnsKelamin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('jns_kelamin');
  }

  static Future<String?> getTtl() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('ttl');
  }

  static Future<String?> getAgama() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('agama');
  }

  static Future<String?> getPicture() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('picturePath');
  }

  static Future<String?> getGender() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('gender');
  }

  static Future<String?> getPhone() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('phone');
  }

  static Future<int?> getId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getInt('id');
  }

  static Future<String?> getEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('email');
  }

  static Future<String?> getNikah() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('nikah');
  }

  static Future<bool?> getLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool('isLogin');
  }

  static void persistToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', token);
  }

  static void persistId(int token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('id', token);
  }

  static void persistName(String name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('name', name);
  }

  static void persistNikah(String name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('nikah', name);
  }

  static void persistLogin(bool name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('isLogin', name);
  }

  static void persistPicture(
    String picturePath,
  ) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('picturePath', picturePath);
  }

  static void persistEmail(String email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('email', email);
  }

  static void persistPhone(String phone) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone', phone);
  }

  static void persistAgama(String agama) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('agama', agama);
  }

  static void persistTtl(String ttl) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('ttl', ttl);
  }

  static void persistKelamin(String jns_kelamin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('jns_kelamin', jns_kelamin);
  }

  static void persistAlamat(String alamat) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('alamat', alamat);
  }

  static void deleteToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  static Future<ApiReturnValue?> login(LoginRequest request) async {
    var response = await client.post(Uri.parse(baseURL + 'login'),
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(<String, String>{
          'email': request.email!,
          'password': request.password!,
        }));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      // User user = User.fromJson(data.result.user);
      User value = User.fromJson(data['result']['user']);
      print(value.nikah);
      persistToken(data['result']['token']);
      persistLogin(true);
      persistName(value.name!);
      persistId(value.id);
      persistNikah(value.nikah!);

      persistEmail(value.email!);
      persistPhone(value.phone!);
      persistAgama(value.agama ?? "Tidak ada");
      persistTtl(value.ttl ?? "Tidak ada");
      persistKelamin(value.gender ?? "Tidak ada");
      persistAlamat(value.alamat ?? "Tidak ada");
      persistPicture(value.url ?? "tidak ada");

      return ApiReturnValue(value: value, statusCode: response.statusCode);
    } else {
      var data = jsonDecode(response.body);
      // print(response.statusCode);
      return ApiReturnValue(
          message: data['message'], statusCode: response.statusCode);
    }
  }

  static Future<ApiReturnValue?> logout() async {
    String? token = await AuthServices.getToken();

    var response = await client.get(
      Uri.parse(baseURL + 'logout'),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token!
      },
    );

    if (response.statusCode == 200) {
      // ignore: unused_local_variable
      var data = jsonDecode(response.body);
      // User user = User.fromJson(data.result.user);
      final pref = await SharedPreferences.getInstance();
      await pref.clear();
      print(data);

      return ApiReturnValue(
          message: "berhasil", statusCode: response.statusCode);
    } else {
      var data = jsonDecode(response.body);
      print(data);

      return ApiReturnValue(
          message: "Somthing wrong", statusCode: response.statusCode);
    }
  }

  static Future<ApiReturnValue?> register(RegisterRequest request) async {
    print(request);
    var response = await client.post(Uri.parse(baseURL + 'register'),
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(<String, String>{
          'name': request.name!,
          'phone': request.phone!,
          'ttl': request.ttl!,
          'jns_kelamin': request.gender!,
          'agama': request.agama!,
          'alamat': request.alamat!,
          'is_married': request.isMerried!,
          'email': request.email!,
          'password': request.password!
        }));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      // print(data);
      print(response.statusCode);

      return ApiReturnValue(message: data['message'], statusCode: 200);
    } else {
      var data = jsonDecode(response.body);
      // print(response.statusCode);
      return ApiReturnValue(
          message: data['message'], statusCode: response.statusCode);
    }
  }

  static Future<ApiReturnValue?> forgotPassword(String email) async {
    var response = await client.post(Uri.parse(baseURL + 'forgot-password'),
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(<String, String>{
          'email': email,
        }));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      // print(data);
      print(response.statusCode);

      return ApiReturnValue(message: data['message'], statusCode: 200);
    } else {
      var data = jsonDecode(response.body);
      // print(response.statusCode);
      return ApiReturnValue(
          message: data['message'], statusCode: response.statusCode);
    }
  }
}
